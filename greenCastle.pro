#-------------------------------------------------
#
# Project created by QtCreator 2014-04-09T19:29:52
#
#-------------------------------------------------

QT       += core gui qml quick opengl script

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

!android: !ios: !blackberry: qtHaveModule(widgets): QT += widgets


TARGET = greenCastle
TEMPLATE = app


SOURCES += main.cpp\
    include/greencastlesingleton.cpp \
    include/mainwindow.cpp \
    include/managers/projectmanagement.cpp \
    include/managers/projectdatamanager.cpp \
    include/managers/projectmatrixmanager.cpp \
    include/managers/projectactivitiesmanager.cpp \
    include/dialogs/aboutgreencastledialog.cpp \
    include/managers/archivementmanager.cpp \
    include/managers/projectmodmanager.cpp \
    include/_testing/_testingwotkspaceform.cpp \
    include/_testing/_testingwelcomeform.cpp \
    include/_testing/_testingpreferencesdialog.cpp \
    include/dependencies/digia/sliding_stacked_widget/sliding_stacked_widget.cpp \
    include/workspace_gui/classicmapeditorform.cpp \
    include/workspace_gui/clicknpointmapeditorform.cpp \
    include/workspace_gui/crossvariables_editor_form.cpp \
    include/workspace_gui/gnurpgmfiledialog.cpp \
    include/workspace_gui/l3dmapeditorform.cpp \
    include/workspace_gui/maps_collection_form.cpp \
    include/workspace_gui/matrix_collection_form.cpp \
    include/workspace_gui/matrixeditorform.cpp \
    include/workspace_gui/matrixviewform.cpp \
    include/workspace_gui/mods_collection_form.cpp \
    include/workspace_gui/newsfeed_form.cpp \
    include/workspace_gui/ressourceslibrariesform.cpp \
    include/workspace_gui/tools_collection_form.cpp \
    include/workspace_gui/widgets/actionprocessingwidget.cpp \
    include/workspace_gui/widgets/actionscollectionform.cpp \
    include/workspace_gui/widgets/dragcheckbox.cpp \
    include/workspace_gui/widgets/pictured_frame.cpp \
    include/gis_stream_object.cpp \
    include/gnuris_interpreter_form.cpp \
    include/gnurisstandalone_mainwindow.cpp \
    include/new_project_form.cpp \
    include/managers/errormanagement.cpp

HEADERS  += \
    include/greencastlesingleton.h \
    include/mainwindow.h \
    include/managers/projectmanagement.h \
    include/managers/projectdatamanager.h \
    include/managers/projectmatrixmanager.h \
    include/managers/projectactivitiesmanager.h \
    include/dialogs/aboutgreencastledialog.h \
    include/managers/archivementmanager.h \
    include/managers/projectmodmanager.h \
    include/_testing/_testingwotkspaceform.h \
    include/_testing/_testingwelcomeform.h \
    include/_testing/_testingpreferencesdialog.h \
    include/dependencies/digia/sliding_stacked_widget/sliding_stacked_widget.h \
    include/workspace_gui/classicmapeditorform.h \
    include/workspace_gui/clicknpointmapeditorform.h \
    include/workspace_gui/crossvariables_editor_form.h \
    include/workspace_gui/gnurpgmfiledialog.h \
    include/workspace_gui/l3dmapeditorform.h \
    include/workspace_gui/maps_collection_form.h \
    include/workspace_gui/matrix_collection_form.h \
    include/workspace_gui/matrixeditorform.h \
    include/workspace_gui/matrixviewform.h \
    include/workspace_gui/mods_collection_form.h \
    include/workspace_gui/newsfeed_form.h \
    include/workspace_gui/ressourceslibrariesform.h \
    include/workspace_gui/tools_collection_form.h \
    include/workspace_gui/widgets/actionprocessingwidget.h \
    include/workspace_gui/widgets/actionscollectionform.h \
    include/workspace_gui/widgets/dragcheckbox.h \
    include/workspace_gui/widgets/pictured_frame.h \
    include/gis_stream_object.h \
    include/gnuris_interpreter_form.h \
    include/gnurisstandalone_mainwindow.h \
    include/new_project_form.h \
    include/managers/errormanagement.h

FORMS    += \
    include/mainwindow.ui \
    include/dialogs/aboutgreencastledialog.ui \
    include/_testing/_testingwotkspaceform.ui \
    include/_testing/_testingwelcomeform.ui \
    include/_testing/_testingpreferencesdialog.ui \
    include/workspace_gui/app_main_menuform.ui \
    include/workspace_gui/classicmapeditorform.ui \
    include/workspace_gui/clicknpointmapeditorform.ui \
    include/workspace_gui/crossvariables_editor_form.ui \
    include/workspace_gui/gnurpgmfiledialog.ui \
    include/workspace_gui/l3dmapeditorform.ui \
    include/workspace_gui/maps_collection_form.ui \
    include/workspace_gui/matrix_collection_form.ui \
    include/workspace_gui/matrixeditorform.ui \
    include/workspace_gui/matrixviewform.ui \
    include/workspace_gui/mods_collection_form.ui \
    include/workspace_gui/newsfeed_form.ui \
    include/workspace_gui/ressourceslibrariesform.ui \
    include/workspace_gui/tools_collection_form.ui \
    include/workspace_gui/widgets/actionprocessingwidget.ui \
    include/workspace_gui/widgets/actionscollectionform.ui \
    include/workspace_gui/widgets/dragcheckbox.ui \
    include/gnurisstandalone_mainwindow.ui \
    include/new_project_form.ui \
    include/gnuris_interpreter_form.ui

RESOURCES += \
    res/image_resources.qrc \
    include/qml/qml_formulars.qrc

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

OTHER_FILES += \
    android/AndroidManifest.xml \
    include/qml/testing_site.qml \
    include/qml/content/best.qml
