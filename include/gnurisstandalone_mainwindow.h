#ifndef GNURISSTANDALONE_MAINWINDOW_H
#define GNURISSTANDALONE_MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class gnurisStandAlone_MainWindow;
}

class gnurisStandAlone_MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit gnurisStandAlone_MainWindow(QWidget *parent = 0);
    ~gnurisStandAlone_MainWindow();
    
private:
    Ui::gnurisStandAlone_MainWindow *ui;
};

#endif // GNURISSTANDALONE_MAINWINDOW_H
