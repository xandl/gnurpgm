#ifndef GIS_STREAM_OBJECT_H
#define GIS_STREAM_OBJECT_H

#include <QString>

/*
 * THis is a very simple version of how Qt's scripting engine can be used,
 * in this case it's not multithreaded so big script with 10k lines will freeze
 * the gui for some time. (Who ever is going to use more than 100 lines per file anyways?...)
 *
 * The GIS (Gnurpgm Installer Script) supports the following functions:
 * (all paths are relative to the project's base dir)
 * -> touch( relative_file_path );
 *    For Example:
 *    -> touch( "res/porn/boobs.jpeg" ); // creates an empty boobs.jpeg file
 *
 * -> download( URL, PREFIX );
 *    The URL should start with "http://", the prefix just contains the folder
 *    Names of where the file should be downloaded; eg "res/music" or "maps/l3d"
 *    For Example:
 *    -> download( "http://www.my-site.wat/porncollection/boys/pic01.jpeg", "res/porn" );
 *    When this file has been downloaded, it will be stored in the folder, represented by the prefix,
 *    with the same name as it had in the remote server (so "http://..../pic1.png" will
 *    be prefix + "pic1.png" on the local filestructure/harddisk
 *
 * -> copy( relativeFilePath, newRelativeFilePath );
 *    The first argument ist the path to __currently existing__ file, where the secound one
 *    is where it's copy is going to.
 *    For Example:
 *    -> copy( "downloads/cool_pic.png", "res/pics/mr_derpy_cool.png");
 *
 * -> copy_from_ex( absoluteFilePath, newRewlativeFilePath );
 *
 *    For Example:
 *    -> copy_from_ex( "C:\\Windows\\System32\\lol.dll", "tmp\\lol.dll" );
 *
 * -> move( relativeFilePath, newRelativeFilePath );
 *    Same as copy(...); but the original file will be deleted after it was
 *    successfully copied.
 *
 * -> delete( relativeFilePath );
 *    Deletes the given file. ".."s will be removed from this given path.
 *    For Example:
 *    -> delete( "downloads/temporary_zip_archive.zip" );
 *
 * -> populate( relativeFilePath, data );
 *    This function is not intended to be used from within this scripting.
 *    It creates the given file the same way as touch(...); does, but it
 *    also writes the given data into this file.
 *    To give this function more sense in this context, you can store Base64 encoded
 *    data within it and it will be decoded afterwards.
 *    For Example:
 *    -> populate( "sys/secret_text.txt", "Hello World" );
 *
 *    For Example, when using Base64 content:
 *    -> populate( "res/soundeffects", "b64", "810s8227s7sd7hiuhasdn273adkqh....." );
 *
 *    As this is a ECMA Script, there is also the possibility to do it this way:
 *    -> var data = "810s8227s7sd7hiuhasdn273adkqh.....";
 *    -> populate( "res/soundeffects", "b64", data );
 *
 *    Security or Virus checks: Pooly none yet... (maybe i should disable this feature...)
 *    Also: all ".."s will be removed from the given filepath.
 *
 * -> unzip( sourceZipFile, extractionLocationPrefix );
 *    the given zip file will be exctracted to the location given by the secound argument.
 *    so the zip file in "download/my_collection.zip" with the following contents:
 *    - pics/pic1.png
 *    - pics/pic2.png
 *    - other/music1.ogg
 *
 *    will become: ( extractionLocationPrefix eg = "res/tests" )
 *    - res/tests/pics/pic1.png
 *    - res/tests/pics/pic2.png
 *    - res/tests/pics/music1.ogg
 *
 * -> mkdir ( relative_folder_name );
 *
 *
*/

//-----------------------------------------------------------------------------
class gis_stream_object
{
/*
 * This Object only helps to build the GIS
 * String in the as-it-should-be way
*/
public:
    gis_stream_object();

    void touch_file( QString relative_path );

    void download_file( QString url, QString target_relative_file_location );

    void copy_file( QString src_relative_filename, QString out_relative_filename );

    void copy_file_from_extern( QString absolute_src_filepath, QString out_relative_filename );

    void move_file( QString src_relative_filename, QString out_relative_filename );

    void delete_file( QString relative_path );

    void populate_file( QString relative_target_filename, QString data );

    void populate_file_with_b64( QString relative_target_filename, QString data );

    void unzip_file( QString sourceZipFile_relative_filename, QString extraction_relativeLocationPrefix );

    void mkdir( QString relative_folder_path );

    QString get_gis();

    void reset_gis_stream();

private:

    QString gis_stream;
};
//-----------------------------------------------------------------------------

#endif // GIS_STREAM_OBJECT_H
