#include "greencastlesingleton.h"

#include <QMenuBar>
#include <QMenu>
#include <QAction>
#include <QApplication>
#include <QDesktopServices>
#include <QUrl>
#include <QFile>
#include <QSaveFile>
#include <QToolBar>
#include <QScreen>

#include "include/dialogs/aboutgreencastledialog.h"
#include "include/_testing/_testingwotkspaceform.h"
#include "include/_testing/_testingwelcomeform.h"

//---------------------------------------------------------------------------
greenCastleSingleton::greenCastleSingleton() :
    QObject(0)
{
    this->_mainwindow = NULL;
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
greenCastleSingleton::~greenCastleSingleton()
{

}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
greenCastleSingleton &greenCastleSingleton::instance()
{
    static greenCastleSingleton singletonObject;
    return singletonObject;
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void greenCastleSingleton::setMainWindowPointer(MainWindow *mw)
{
    this->_mainwindow = mw;
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void greenCastleSingleton::initialize()
{
    this->project_management = new projectManagement(this);

    // Any application-arguments?
    if (qApp->arguments().contains("-o"))
    {
        // open the .gnurpgm file on the given path
    }
    else
    {
        // clean start
        _setMainWindowMenu_to_preProjektMode();
        _mainwindow->gotoForm( MainWindow::WELCOME_PAGE );

        if ( _mainwindow->devicePixelRatio() == 2 )
        {
            // Retina Display time to HiDpi!
            _mainwindow->toolbar()->setIconSize(QSize(46,46));
        }
    }
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void greenCastleSingleton::_setMainWindowMenu_to_preProjektMode()
{
    QMenuBar* menubar = new QMenuBar();

    QMenu* m_file = new QMenu(tr("File"));
    m_file->addAction( QIcon(), tr("Resume previous Project"), _mainwindow->welcomeForm, SLOT(resumePreviousProject_clicked()));
    m_file->addAction( QIcon(), tr("Create new Project"), _mainwindow->welcomeForm, SLOT(openNewProjectAssistant_clicked()));
    m_file->addAction( QIcon(), tr("Open Project from File"), _mainwindow->welcomeForm, SLOT(openProjectFromFileDialog_clicked()));

#ifndef Q_OS_MAC
    m_file->addSeparator();
#endif

    m_file->addAction( QIcon(), tr("Quit"), this, SLOT(shutdown()));

    QMenu* m_network = new QMenu(tr("Network"));
    m_network->addAction( QIcon(), tr("Clone Project from Versioncontrol"), _mainwindow->welcomeForm, SLOT(cloneProject_clicked()));
    m_network->addAction( QIcon(), tr("Join Project over LAN"), _mainwindow->welcomeForm, SLOT(joinLan_clicked()));


    QMenu* m_about = new QMenu(tr("About"));
    m_about->addAction( QIcon(), tr("Visit gnurpgm.sf.net"), this, SLOT(visitGnurpgmSfNet()));
    m_about->addAction( QIcon(), tr("Visit gnurpgm forums"), this, SLOT(visitGnurpgmForums()));

#ifndef Q_OS_MAC
    m_file->addSeparator();
#endif

    m_about->addAction( QIcon(), tr("About gnurpgm"), this, SLOT(aboutGnurpgmDialog()));

#ifndef Q_OS_MAC
    m_file->addSeparator();
#endif

    m_about->addAction( QIcon(), tr("Preferences"), this, SLOT(show_PreferencesDialogSheet()));


    menubar->addMenu( m_file );
    menubar->addMenu( m_network );
    menubar->addMenu( m_about );

    _mainwindow->setMenuBar( menubar );
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void greenCastleSingleton::_setMainWindowMenu_to_projektMode()
{

}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void greenCastleSingleton::aboutGnurpgmDialog()
{
    aboutGreenCastleDialog* dg = new aboutGreenCastleDialog(_mainwindow);
    dg->exec();
    delete dg;
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void greenCastleSingleton::visitGnurpgmSfNet()
{
    QDesktopServices::openUrl(QUrl("http://gnurpgm.sf.net"));
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void greenCastleSingleton::visitGnurpgmForums()
{
    QDesktopServices::openUrl(QUrl("http://gnurpgm.sf.net"));
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
void greenCastleSingleton::shutdown()
{
    // save things?
    qApp->quit();
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
QPair<bool,QString> greenCastleSingleton::_writeDataToFile(QString absoluteFilename, QByteArray data)
{
    QSaveFile f(absoluteFilename);
    if ( f.open(QIODevice::WriteOnly) )
    {
        f.write( data );

        if ( !f.commit() )
        {
            return QPair<bool,QString>(false, f.errorString());
        }
    }
    else
        return QPair<bool,QString>(false, f.errorString());

    return QPair<bool,QString>(true,"");
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
QPair<bool,QString> greenCastleSingleton::_readDataFromFile(QString absoluteFilename, QByteArray *dataPointer)
{
    QFile f(absoluteFilename);
    if (f.open(QIODevice::ReadOnly))
    {
        *dataPointer = f.readAll();
        f.close();
    }
    else
        return QPair<bool,QString>(false, f.errorString());

    return QPair<bool, QString>(true, "");
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void greenCastleSingleton::setMainWindowToolbar(QList<QPair<bool, QAction *> > actions)
{
    if (!_mainwindow->toolbar()->actions().isEmpty())
        if (_mainwindow->toolbar()->actions().first()->actionGroup() != 0)
            _mainwindow->toolbar()->actions().first()->actionGroup()->deleteLater();

    _mainwindow->toolbar()->clear();

    for ( int i = 0; i < actions.count(); i++)
    {
        QPair<bool, QAction *> pair = actions[i];

        // is at a spacer?
        if (pair.first)
        {
            // it's a spacer item
            QWidget* spacer = new QWidget();
            spacer->setSizePolicy( QSizePolicy::MinimumExpanding, QSizePolicy::Preferred );
            _mainwindow->toolbar()->addWidget(spacer);
        }
        else
        {
            // it's an actual item/action
            _mainwindow->toolbar()->addAction(pair.second);
        }
    }
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void greenCastleSingleton::show_PreferencesDialogSheet()
{
    _mainwindow->gotoForm(MainWindow::PREFERENCES);
}
//---------------------------------------------------------------------------

