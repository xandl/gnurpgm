#ifndef MATRIX_COLLECTION_FORM_H
#define MATRIX_COLLECTION_FORM_H

#include <QWidget>

namespace Ui {
class matrix_collection_form;
}

class matrix_collection_form : public QWidget
{
    Q_OBJECT
    
public:
    explicit matrix_collection_form(QWidget *parent = 0);
    ~matrix_collection_form();
    
private:
    Ui::matrix_collection_form *ui;
};

#endif // MATRIX_COLLECTION_FORM_H
