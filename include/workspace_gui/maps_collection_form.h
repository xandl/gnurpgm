#ifndef MAPS_COLLECTION_FORM_H
#define MAPS_COLLECTION_FORM_H

#include <QWidget>
#include <QPointer>
#include <QListWidgetItem>
#include <QGraphicsBlurEffect>

//-----------------------------------------------------------------------------
namespace Ui {
class maps_collection_form;
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
class maps_collection_form : public QWidget
{
    Q_OBJECT

public:
    explicit maps_collection_form(QWidget *parent = 0);
    ~maps_collection_form();

private:
    QSize target_map_previews_size;
    int target_map_next_index;
    bool continue_loading_map_previews;

public slots:

    void filter_btn_clicked();

    void refresh_maps_list();

    void delete_map( QString absolute_map_path );

private slots:

    void on_pushButton_zoom_bigger_clicked();

    void on_pushButton_zoom_normal_clicked();

    void on_pushButton_zoom_smaller_clicked();

    void on_pushButton_new_map_clicked();

    void kick_refresh_next_map_icon_tick();

    void refresh_next_map_icon_tick();

    void on_pushButton_new_map_close_clicked();

    void recheck_map_name( QString arg1 );

    void map_context_menu_requested(QPoint where);

    void on_listWidget_maps_itemClicked(QListWidgetItem *item);

private:
    Ui::maps_collection_form *ui;

signals:
    void open_map_pls( QString absolute_path );
};
//-----------------------------------------------------------------------------

#endif // MAPS_COLLECTION_FORM_H
