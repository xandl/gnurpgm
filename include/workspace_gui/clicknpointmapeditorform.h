#ifndef CLICKNPOINTMAPEDITORFORM_H
#define CLICKNPOINTMAPEDITORFORM_H

#include <QWidget>

namespace Ui {
class clickNpointMapEditorForm;
}

class clickNpointMapEditorForm : public QWidget
{
    Q_OBJECT
    
public:
    explicit clickNpointMapEditorForm(QWidget *parent = 0);
    ~clickNpointMapEditorForm();
    
private:
    Ui::clickNpointMapEditorForm *ui;
};

#endif // CLICKNPOINTMAPEDITORFORM_H
