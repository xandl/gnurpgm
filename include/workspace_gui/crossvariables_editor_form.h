#ifndef CROSSVARIABLES_EDITOR_FORM_H
#define CROSSVARIABLES_EDITOR_FORM_H

#include <QWidget>

namespace Ui {
class crossvariables_editor_form;
}

class crossvariables_editor_form : public QWidget
{
    Q_OBJECT
    
public:
    explicit crossvariables_editor_form(QWidget *parent = 0);
    ~crossvariables_editor_form();
    
private:
    Ui::crossvariables_editor_form *ui;
};

#endif // CROSSVARIABLES_EDITOR_FORM_H
