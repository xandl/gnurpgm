#ifndef MODS_COLLECTION_FORM_H
#define MODS_COLLECTION_FORM_H

#include <QWidget>

namespace Ui {
class mods_collection_form;
}

class mods_collection_form : public QWidget
{
    Q_OBJECT
    
public:
    explicit mods_collection_form(QWidget *parent = 0);
    ~mods_collection_form();
    
private:
    Ui::mods_collection_form *ui;
};

#endif // MODS_COLLECTION_FORM_H
