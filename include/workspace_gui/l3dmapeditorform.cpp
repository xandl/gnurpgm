#include "l3dmapeditorform.h"
#include "ui_l3dmapeditorform.h"

l3dMapEditorForm::l3dMapEditorForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::l3dMapEditorForm)
{
    ui->setupUi(this);
}

l3dMapEditorForm::~l3dMapEditorForm()
{
    delete ui;
}
