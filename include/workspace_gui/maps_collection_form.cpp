#include "maps_collection_form.h"
#include "ui_maps_collection_form.h"

#include <QDir>
#include <QTimer>
#include <QMessageBox>

#include "include/greencastlesingleton.h"

//-----------------------------------------------------------------------------
maps_collection_form::maps_collection_form(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::maps_collection_form)
{
    ui->setupUi(this);

    this->ui->stackedWidget->setCurrentIndex( 0 );

    this->target_map_next_index = 0;

    this->continue_loading_map_previews = false;

    this->ui->listWidget_maps->setContextMenuPolicy( Qt::CustomContextMenu );

    connect( ui->lineEdit_map_name, SIGNAL(textChanged(QString)),
             this, SLOT(recheck_map_name(QString)) );

    connect( ui->listWidget_maps, SIGNAL(customContextMenuRequested(QPoint) ),
             this, SLOT(map_context_menu_requested(QPoint)) );

    QList<QPair<QString, QString> > list;

    list.append( QPair<QString,QString>( tr("Show all" ), ":/res/img/btn_maps.png" ) );
    list.append( QPair<QString,QString>( tr("Show classic" ), ":/res/img/btn_maps.png" ) );
    list.append( QPair<QString,QString>( tr("Show click'n'point" ), ":/res/img/btn_maps.png" ) );
    list.append( QPair<QString,QString>( tr("Show 3D" ), ":/res/img/btn_maps.png" ) );

    ui->onOffwidget_filters->setBtnsIconSize( QSize( 22,22 ) );

    ui->onOffwidget_filters->setBtns( list );

    connect( ui->onOffwidget_filters, SIGNAL(some_button_clicked()),
             this, SLOT(filter_btn_clicked()) );

    ui->onOffwidget_filters->setBtnChecked( tr("Show all" ) );

    // just to be sure that the listWidget has some iconSize value been set
    this->on_pushButton_zoom_normal_clicked();
    ui->onOffwidget_filters->setBtnChecked( tr("Show all"), true );

    // go!
    // workspace_widget must do this, because
    // system_sync_hub::instanz().current_project
    // doesnt exist yet
    //this->refresh_maps_list();
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
maps_collection_form::~maps_collection_form()
{
    delete ui;
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void maps_collection_form::filter_btn_clicked()
{
    if ( ui->onOffwidget_filters->isBtnChecked( tr("Show all") ) )
    {
        for ( int i = 0; i < ui->listWidget_maps->count(); i++ )
            ui->listWidget_maps->item( i )->setHidden( false );
    }
    else if ( ui->onOffwidget_filters->isBtnChecked( tr("Show classic") ) )
    {
        for ( int i = 0; i < ui->listWidget_maps->count(); i++ )
            ui->listWidget_maps->item( i )->setHidden(
                        !ui->listWidget_maps->item( i )->statusTip().contains("classicmap") );
    }
    else if ( ui->onOffwidget_filters->isBtnChecked( tr("Show click'n'point") ) )
    {
        for ( int i = 0; i < ui->listWidget_maps->count(); i++ )
            ui->listWidget_maps->item( i )->setHidden(
                        !ui->listWidget_maps->item( i )->statusTip().contains("clicknpointmap") );
    }
    else if ( ui->onOffwidget_filters->isBtnChecked( tr("Show 3D") ) )
    {
        for ( int i = 0; i < ui->listWidget_maps->count(); i++ )
            ui->listWidget_maps->item( i )->setHidden(
                        !ui->listWidget_maps->item( i )->statusTip().contains("3dmap") );
    }
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void maps_collection_form::refresh_maps_list()
{
    this->ui->listWidget_maps->clear();

    Q_ASSERT_X( gnurpgm.project_management->currentProject() != 0,
                "maps_collection_form::refresh_maps_list()",
                "gnurpgm.project_management->currentProject() == 0");

    QDir d( gnurpgm.project_management->currentProject()->project_base_dir
            + QDir::separator() + "maps" );

    QStringList maps = d.entryList( QStringList() << QString("*.map"),
                                QDir::Files | QDir::NoDotAndDotDot,
                                QDir::Name );

    foreach ( QString mapname, maps )
    {
        QListWidgetItem* it = new QListWidgetItem();
        it->setToolTip( mapname );
        it->setData( Qt::UserRole, QVariant( d.absolutePath() + QDir::separator() + mapname ) );
        it->setStatusTip( "classicmap" );

        this->ui->listWidget_maps->addItem( it );
    }

    if ( ui->listWidget_maps->iconSize() == QSize( 256, 256 ) )
        on_pushButton_zoom_bigger_clicked();

    else if ( ui->listWidget_maps->iconSize() == QSize( 128, 128 ) )
        on_pushButton_zoom_normal_clicked();

    else
        on_pushButton_zoom_smaller_clicked();

    this->filter_btn_clicked();
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void maps_collection_form::on_pushButton_zoom_bigger_clicked()
{
    this->continue_loading_map_previews = false;
    this->target_map_next_index = 0;
    this->target_map_previews_size = QSize( 256,256 );
    ui->listWidget_maps->setIconSize( this->target_map_previews_size );

    QTimer::singleShot( 200, this, SLOT(kick_refresh_next_map_icon_tick() ) );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void maps_collection_form::on_pushButton_zoom_normal_clicked()
{
    this->continue_loading_map_previews = false;
    this->target_map_next_index = 0;
    this->target_map_previews_size = QSize( 128,128 );
    ui->listWidget_maps->setIconSize( this->target_map_previews_size );

    QTimer::singleShot( 200, this, SLOT(kick_refresh_next_map_icon_tick() ) );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void maps_collection_form::on_pushButton_zoom_smaller_clicked()
{
    this->continue_loading_map_previews = false;
    this->target_map_next_index = 0;
    this->target_map_previews_size = QSize( 64,64 );
    ui->listWidget_maps->setIconSize( this->target_map_previews_size );

    QTimer::singleShot( 200, this, SLOT(kick_refresh_next_map_icon_tick() ) );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void maps_collection_form::on_pushButton_new_map_clicked()
{
    ui->page_maps_collection->setGraphicsEffect(new QGraphicsBlurEffect());
    ui->page_maps_collection->update();

    ui->frame_new_map_bg->set_bg_img( QPixmap::grabWidget( ui->page_maps_collection ).toImage() );

    delete ui->page_maps_collection->graphicsEffect();

    ui->stackedWidget->setCurrentIndex( ui->stackedWidget->indexOf( ui->page_new_map ) );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void maps_collection_form::kick_refresh_next_map_icon_tick()
{
    this->continue_loading_map_previews = true;
    QTimer::singleShot( 10, this, SLOT(refresh_next_map_icon_tick() ) );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void maps_collection_form::refresh_next_map_icon_tick()
{
    // we'll be still doing this, or did we finish already?
    if ( this->target_map_next_index == ui->listWidget_maps->count()
         || !this->continue_loading_map_previews )
        return; // nothing to do

    // lets get some fresh meat
    QString map_file = ui->listWidget_maps->item( this->target_map_next_index )->data( Qt::UserRole ).toString();

    QPixmap preview( this->target_map_previews_size );
    QImage img;

    if ( preview.load( map_file + ".preview" ) )
        img = preview.toImage();

    else
        // map_preview_404.png has a original size of 256x256
        img = QImage( ":/res/img/map_preview_404.png" );


    preview = QPixmap::fromImage( img.scaled( this->target_map_previews_size,
                                              Qt::KeepAspectRatio,
                                              Qt::SmoothTransformation ) );

//    preview = system_sync_hub::get_blured_text_on_pixmap( preview, ui->listWidget_maps->item( this->target_map_next_index )->toolTip(),
//                                                          QRect( 0, 0, this->target_map_previews_size.width(), this->target_map_previews_size.height() ));


    ui->listWidget_maps->item( this->target_map_next_index )->setSizeHint( this->target_map_previews_size );
    ui->listWidget_maps->item( this->target_map_next_index )->setIcon( QIcon( preview ) );

    this->target_map_next_index++;
    QTimer::singleShot( 100, this, SLOT(refresh_next_map_icon_tick() ) );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void maps_collection_form::on_pushButton_new_map_close_clicked()
{
    ui->stackedWidget->setCurrentIndex( ui->stackedWidget->indexOf( ui->page_maps_collection ) );
    ui->lineEdit_map_name->clear();
    ui->toolButton_3dtype_tiles->setChecked( false );
    ui->toolButton_3dtype_freestyle->setChecked( false );
    ui->toolButton_3dtype_voxel->setChecked( false );
    ui->toolButton_maptype_classic->setChecked( false );
    ui->toolButton_maptype_cnp->setChecked( false );
    ui->toolButton_maptype_3d->setChecked( false );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void maps_collection_form::recheck_map_name(QString arg1)
{
    bool map_name_already_taken = false;
    for ( int i = 0; i < ui->listWidget_maps->count(); i++ )
    {
        if ( !arg1.isEmpty()
             && ui->listWidget_maps->item( i )->text().trimmed() == arg1.trimmed() )
        {
            map_name_already_taken = true;
            break;
        }
    }

    if ( map_name_already_taken )
    {
        ui->toolButton_nameOk_check->setToolTip( tr("This name is already taken!") );
        ui->toolButton_nameOk_check->setIcon( QIcon( ":/res/img/close_red.png" ) );
    }
    else
    {
        ui->toolButton_nameOk_check->setToolTip( tr("This name reads fancy :)") );
        ui->toolButton_nameOk_check->setIcon( QIcon( ":/res/img/ok.png" ) );
    }

}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void maps_collection_form::map_context_menu_requested(QPoint where)
{
    // what has been clicked?
    QListWidgetItem* it = ui->listWidget_maps->itemAt( where );

    if ( it == 0 )
        return;

    if ( QMessageBox::question( 0, tr("Delete map"), tr("Do you really want to delete map %1?").arg( it->toolTip() ),
                                QMessageBox::Yes, QMessageBox::No )
         == QMessageBox::Yes )
    {
        this->delete_map( it->data( Qt::UserRole ).toString() );
        this->refresh_maps_list();
    }
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void maps_collection_form::delete_map(QString absolute_map_path)
{
    QFile f( absolute_map_path );

    // delete the map file
    if ( f.remove() )
    {
        // delete the map's preview file
        f.setFileName( f.fileName() + ".preview" );
        if ( f.exists() )
            f.remove(); // i dont really care if this fails
    }
    else
        QMessageBox::critical( 0, tr("Remove file error"), tr("Couldn't delete map file: %1, because: %2").arg( f.fileName() ).arg( f.errorString() ) );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void maps_collection_form::on_listWidget_maps_itemClicked(QListWidgetItem *item)
{
    emit this->open_map_pls( item->data( Qt::UserRole ).toString() );
}
//-----------------------------------------------------------------------------
