#ifndef MATRIXEDITORFORM_H
#define MATRIXEDITORFORM_H

#include <QWidget>

namespace Ui {
class matrixEditorForm;
}

class matrixEditorForm : public QWidget
{
    Q_OBJECT
    
public:
    explicit matrixEditorForm(QWidget *parent = 0);
    ~matrixEditorForm();
    
private:
    Ui::matrixEditorForm *ui;
};

#endif // MATRIXEDITORFORM_H
