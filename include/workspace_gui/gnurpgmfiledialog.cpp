#include "gnurpgmfiledialog.h"
#include "ui_gnurpgmfiledialog.h"

gnurpgmFileDialog::gnurpgmFileDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::gnurpgmFileDialog)
{
    ui->setupUi(this);
}

gnurpgmFileDialog::~gnurpgmFileDialog()
{
    delete ui;
}
