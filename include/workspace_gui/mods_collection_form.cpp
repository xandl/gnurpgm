#include "mods_collection_form.h"
#include "ui_mods_collection_form.h"

mods_collection_form::mods_collection_form(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::mods_collection_form)
{
    ui->setupUi(this);
}

mods_collection_form::~mods_collection_form()
{
    delete ui;
}
