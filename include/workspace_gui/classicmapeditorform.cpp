#include "classicmapeditorform.h"
#include "ui_classicmapeditorform.h"

classicMapEditorForm::classicMapEditorForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::classicMapEditorForm)
{
    ui->setupUi(this);
}

classicMapEditorForm::~classicMapEditorForm()
{
    delete ui;
}
