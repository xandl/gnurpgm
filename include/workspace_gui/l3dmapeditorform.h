#ifndef L3DMAPEDITORFORM_H
#define L3DMAPEDITORFORM_H

#include <QWidget>

namespace Ui {
class l3dMapEditorForm;
}

class l3dMapEditorForm : public QWidget
{
    Q_OBJECT
    
public:
    explicit l3dMapEditorForm(QWidget *parent = 0);
    ~l3dMapEditorForm();
    
private:
    Ui::l3dMapEditorForm *ui;
};

#endif // L3DMAPEDITORFORM_H
