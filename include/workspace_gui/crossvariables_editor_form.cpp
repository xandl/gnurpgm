#include "crossvariables_editor_form.h"
#include "ui_crossvariables_editor_form.h"

crossvariables_editor_form::crossvariables_editor_form(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::crossvariables_editor_form)
{
    ui->setupUi(this);
}

crossvariables_editor_form::~crossvariables_editor_form()
{
    delete ui;
}
