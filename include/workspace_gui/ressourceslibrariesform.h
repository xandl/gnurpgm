#ifndef RESSOURCESLIBRARIESFORM_H
#define RESSOURCESLIBRARIESFORM_H

#include <QWidget>

namespace Ui {
class ressourcesLibrariesForm;
}

class ressourcesLibrariesForm : public QWidget
{
    Q_OBJECT
    
public:
    explicit ressourcesLibrariesForm(QWidget *parent = 0);
    ~ressourcesLibrariesForm();
    
private:
    Ui::ressourcesLibrariesForm *ui;
};

#endif // RESSOURCESLIBRARIESFORM_H
