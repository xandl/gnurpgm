#ifndef GNURPGMFILEDIALOG_H
#define GNURPGMFILEDIALOG_H

#include <QDialog>

namespace Ui {
class gnurpgmFileDialog;
}

class gnurpgmFileDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit gnurpgmFileDialog(QWidget *parent = 0);
    ~gnurpgmFileDialog();
    
private:
    Ui::gnurpgmFileDialog *ui;
};

#endif // GNURPGMFILEDIALOG_H
