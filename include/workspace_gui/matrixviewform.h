#ifndef MATRIXVIEWFORM_H
#define MATRIXVIEWFORM_H

#include <QWidget>

namespace Ui {
class MatrixViewForm;
}

class MatrixViewForm : public QWidget
{
    Q_OBJECT
    
public:
    explicit MatrixViewForm(QWidget *parent = 0);
    ~MatrixViewForm();
    
private:
    Ui::MatrixViewForm *ui;
};

#endif // MATRIXVIEWFORM_H
