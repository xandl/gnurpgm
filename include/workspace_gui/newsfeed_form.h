#ifndef NEWSFEED_FORM_H
#define NEWSFEED_FORM_H

#include <QWidget>

namespace Ui {
class newsfeed_form;
}

class newsfeed_form : public QWidget
{
    Q_OBJECT

public:
    explicit newsfeed_form(QWidget *parent = 0);
    ~newsfeed_form();

private slots:
    void on_pushButton_show_filter_clicked();

private:
    Ui::newsfeed_form *ui;
};

#endif // NEWSFEED_FORM_H
