#include "newsfeed_form.h"
#include "ui_newsfeed_form.h"

newsfeed_form::newsfeed_form(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::newsfeed_form)
{
    ui->setupUi(this);

    ui->comboBox_filters->setVisible( false );
}

newsfeed_form::~newsfeed_form()
{
    delete ui;
}

void newsfeed_form::on_pushButton_show_filter_clicked()
{
    ui->comboBox_filters->setVisible( !ui->comboBox_filters->isVisible() );
}
