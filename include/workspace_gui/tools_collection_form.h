#ifndef TOOLS_COLLECTION_FORM_H
#define TOOLS_COLLECTION_FORM_H

#include <QWidget>

namespace Ui {
class tools_collection_form;
}

class tools_collection_form : public QWidget
{
    Q_OBJECT
    
public:
    explicit tools_collection_form(QWidget *parent = 0);
    ~tools_collection_form();
    
private:
    Ui::tools_collection_form *ui;
};

#endif // TOOLS_COLLECTION_FORM_H
