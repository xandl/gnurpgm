#include "matrixviewform.h"
#include "ui_matrixviewform.h"

MatrixViewForm::MatrixViewForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MatrixViewForm)
{
    ui->setupUi(this);
}

MatrixViewForm::~MatrixViewForm()
{
    delete ui;
}
