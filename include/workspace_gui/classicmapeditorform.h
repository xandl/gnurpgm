#ifndef CLASSICMAPEDITORFORM_H
#define CLASSICMAPEDITORFORM_H

#include <QWidget>

namespace Ui {
class classicMapEditorForm;
}

class classicMapEditorForm : public QWidget
{
    Q_OBJECT
    
public:
    explicit classicMapEditorForm(QWidget *parent = 0);
    ~classicMapEditorForm();
    
private:
    Ui::classicMapEditorForm *ui;
};

#endif // CLASSICMAPEDITORFORM_H
