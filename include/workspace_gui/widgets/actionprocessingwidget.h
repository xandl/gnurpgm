#ifndef ACTIONPROCESSINGWIDGET_H
#define ACTIONPROCESSINGWIDGET_H

#include <QWidget>
#include <QtGlobal>
#include <QFile>
#include <QDir>
#include <QLabel>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QDebug>
#include <QBuffer>

//#include "include/dependencies/quazip/quazip.h"
//#include "include/dependencies/quazip/quazipfile.h"

//-----------------------------------------------------------------------------
namespace Ui {
class ActionProcessingWidget;
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
class ActionProcessingWidget : public QWidget
{
    Q_OBJECT

public:

    //-----------------------------------------------------------------------------
    struct ACTION_INSTRUCTION_STRUCT
    {
        //-----------------------------------------------------------------------------
        enum instruction_types
        {
            //
            // The Developer sucks in this case.
            //
            NONE = 0,

            // When the _Download_ type has been applied,
            // the progress bar will represent how much of the downloaded data
            // has been received.
            // and also there have to be 2 arguments given:
            // args[0](QString) Source Url
            // args[1](QString) Download local path
            DOWNLOAD = 1,

            // When the _TOUCH_ type has been applied,
            // an empty file will be created, whose absolute location
            // is beeing defined by
            // args[0](QString) absolute file path
            TOUCH = 2,

            // When the _COPY_ type has been applied,
            // a file will be copied to another location
            // args[0](QString) absolute source file path
            // args[1](QString) absolute new file path
            COPY = 3,

            // When the _COPY_ type has been applied,
            // a file will be copied to another location
            // args[0](QString) absolute (out of the project) source file path
            // args[1](QString) absolute new file path
            COPY_EX = 4,

            // When the _MOVE_ type has been applied,
            // a file will be copied to another location
            // and gets deleted afterwards
            // args[0](QString) absolute source file path
            // args[1](QString) absolute new file path
            MOVE = 5,

            // When the _DELETE_ type has been applied,
            // the file defined in
            // args[0](QString) absolute target filepath
            // will be deleted
            DELETE = 6,

            // This one is the tickiest one of the above.
            // When the _POPULATE_ type has been applied,
            // a file will be _TOUCH_ed, defined by args[0](QString)
            // and afterwards the bytewise data in args[1](QByteArray)
            // will be written into it
            POPULATE = 7,

            // Unzips a standard zip archive
            // (or whatever quazip supports)
            // to a given path
            // the folder structure will be kept as it is
            // so for example:
            // zip content:
            //  /index.html
            //  /img/
            //  /img/pic1.png
            //  /img/pic2.png
            //
            // result on local folder structure:
            // /path/to/project/__prefix__/
            // /path/to/project/__prefix__/index.html
            // /path/to/project/__prefix__/img/pic1.png
            // /path/to/project/__prefix__/img/pic2.png
            //
            // so the following args will be expected:
            // args[0] (QString) what zip archive?
            // args[1] (QString) Prefix (for example: "res/pictures")
            UNZIP = 8,

            MKDIR = 9


        }
        //-----------------------------------------------------------------------------
        instruction_type;

        QVariantList args;

        QByteArray DownloadedData;
    }
    //-----------------------------------------------------------------------------
    action_instruction;

    ActionProcessingWidget(ACTION_INSTRUCTION_STRUCT instructions);
    ActionProcessingWidget(ACTION_INSTRUCTION_STRUCT::instruction_types instruction_type, QVariantList args);
    ~ActionProcessingWidget();

    enum ENGAGE_RESULTS
    {
        EXECUTE_ME_PLS,
        ENGAGE_SUCCEDED,
        ENGAGE_FAILED
    }
    engagement_result;

    QString error_msg;

    QLabel* text_label_pointer;

    QNetworkAccessManager* download_manager;

    void setup();

    /*
     * init_sleeping_state(); interprets the currently loaded action_instruction
     * setting, the gui up to display a minimum gui, showing in the gui what this action
     * is going to do, despite it aint doing anything - yet
    */
        void init_sleeping_state();


    /*
     * The actionsCollectionWidget Thing tried to execute this state,
     * but something failed. so we show a red background with a white font now.
     * init_sleeping_state(); turns this back to the "normal" style
    */
        void init_error_state(); // just something went wrong
        void init_error_state( QString what_went_wrong );
        void init_error_state( QString what_went_wrong, QString what_to_tell_the_user );


        void init_succeeded_state();

    /*
     * init_engage(); actually starts the action defined in this->action_instruction
     * and keeps the user up to date by updating it's progressbar
    */
        void init_engage();

    signals:
        void finished(); // so the actionsCollectionForm knows to move on

        void failed( QString error_msg ); // so the actionsCollectionForm knows to move on anyways...

    private slots:
        void on_toolButton_restart_clicked();

        void on_toolButton_pause_clicked();

        void on_toolButton_stop_clicked();

        void download_proceeds(qint64 received_bytes, qint64 max_bytes);

        void download_finished(QNetworkReply* rep);

private:
    Ui::ActionProcessingWidget *ui;
};
//-----------------------------------------------------------------------------

#endif // ACTIONPROCESSINGWIDGET_H
