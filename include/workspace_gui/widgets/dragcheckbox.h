#ifndef DRAGCHECKBOX_H
#define DRAGCHECKBOX_H

#include <QWidget>
#include <QPushButton>
#include <QPair>
#include <QHBoxLayout>

//-----------------------------------------------------------------------------
namespace Ui {
class dragCheckBox;
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
class dragCheckBox : public QWidget
{
    Q_OBJECT
    
public:
    explicit dragCheckBox(QWidget *parent = 0);
    ~dragCheckBox();

    enum btn_check_modes
    {
        ONLY_CHECK_A_SINGLE_BTN_AT_A_TIME,
        MULTIPLE_CHECKS_AT_A_TIME_ARE_OKAY
    }
    btn_check_mode;

    void setBtnsCheckMode( btn_check_modes mode );

    void setBtns( QStringList btn_texts );

    void setBtns( QList< QPair<QString,QString> > btn_texts_and_icon_paths );
    
    void setBtnChecked( QString btn_text );

    void setBtnChecked( QString btn_text, bool b );

    void setBtnsIconSize( QSize icon_size );

    bool isBtnChecked( QString btn_text );

    const QList<QPushButton*> get_buttons();

private:
    QList<QPushButton*> buttons;

    QHBoxLayout* ui_layout;

    // only interessting for single
    // checkable collections
    QPushButton* prev_checked_btn;

private slots:
    void check_btns_policy_on_click();

signals:
    void some_button_clicked();

private:
    Ui::dragCheckBox *ui;
};
//-----------------------------------------------------------------------------

#endif // DRAGCHECKBOX_H
