#include "dragcheckbox.h"
#include "ui_dragcheckbox.h"

//-----------------------------------------------------------------------------
dragCheckBox::dragCheckBox(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::dragCheckBox)
{
    ui->setupUi(this);

    this->btn_check_mode = ONLY_CHECK_A_SINGLE_BTN_AT_A_TIME;

    this->ui_layout = new QHBoxLayout();
    this->ui_layout->setContentsMargins( 0,0,0,0 );

    this->setLayout( ui_layout );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
dragCheckBox::~dragCheckBox()
{
    delete ui;
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void dragCheckBox::setBtns(QStringList btn_texts)
{
    QList< QPair<QString,QString> > btn_texts_without_icon_shit;
    foreach( QString btn_text, btn_texts )
        btn_texts_without_icon_shit.append( QPair<QString,QString>( btn_text, QString() ) );


    this->setBtns( btn_texts_without_icon_shit );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void dragCheckBox::setBtns(QList<QPair<QString, QString> > btn_texts_and_icon_paths)
{
    while ( !this->buttons.isEmpty() )
        delete this->buttons.takeFirst();

    for ( int i = 0; i < btn_texts_and_icon_paths.count(); i++ )
    {
        QPushButton* new_btn = new QPushButton( btn_texts_and_icon_paths.at(i).first );
        new_btn->setCheckable( true );
        new_btn->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );

        if ( !btn_texts_and_icon_paths.at(i).second.isEmpty() )
            new_btn->setIcon( QIcon( btn_texts_and_icon_paths.at(i).second ) );

        connect( new_btn, SIGNAL(clicked()),
                 this, SLOT(check_btns_policy_on_click()) );

        connect( new_btn, SIGNAL(clicked()),
                 this, SIGNAL(some_button_clicked()) );

        this->ui_layout->addWidget( new_btn );
        this->buttons.append( new_btn );
    }
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void dragCheckBox::setBtnsCheckMode(btn_check_modes mode)
{
    this->btn_check_mode = mode;
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void dragCheckBox::setBtnChecked(QString btn_text)
{
    this->setBtnChecked( btn_text, true );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void dragCheckBox::setBtnChecked(QString btn_text, bool b)
{
    foreach( QPushButton* btn, buttons )
    {
        if ( btn->text() == btn_text )
        {
            btn->setChecked( b );
            return;
        }
    }
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
bool dragCheckBox::isBtnChecked(QString btn_text)
{
    foreach( QPushButton* btn, buttons )
    {
        if ( btn->text() == btn_text )
        {
            return btn->isChecked();
        }
    }
    return false;
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void dragCheckBox::check_btns_policy_on_click()
{
    QObject* obj = sender();

    // if this slot has been called without
    // an signal, obj will be 0

    if ( obj != 0 )
    {
        QPushButton* btn = static_cast<QPushButton*>(obj);

        // if MULTIPLE_CHECKS_AT_A_TIME_ARE_OKAY
        // we dont care if we had to uncheck all other btns

        if ( this->btn_check_mode == dragCheckBox::ONLY_CHECK_A_SINGLE_BTN_AT_A_TIME )
        {
            // actually, if the sender is the same btn as the btn thats currently
            // checked, well ignore dis shitz
            QString btn_text = btn->text();

            for ( int i = 0; i < buttons.count(); i++ )
                if ( buttons.at(i)->text() != btn_text )
                    buttons.at(i)->setChecked( false );

        }
    }
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
const QList<QPushButton*> dragCheckBox::get_buttons()
{
    return buttons;
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void dragCheckBox::setBtnsIconSize(QSize icon_size)
{
    for ( int i = 0; i < buttons.count(); i++ )
        buttons[i]->setIconSize( icon_size );
}
//-----------------------------------------------------------------------------
