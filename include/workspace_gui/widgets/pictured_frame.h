#ifndef PICTURED_FRAME_H
#define PICTURED_FRAME_H

#include <QFrame>
#include <QImage>
#include <QPainter>
#include <QPaintEvent>

class pictured_frame : public QFrame
{
    Q_OBJECT
public:
    explicit pictured_frame(QWidget *parent = 0);

    void set_bg_img( QImage img );

private:
    QImage bg_image;

protected:
    void paintEvent(QPaintEvent *ev);
    
};

#endif // PICTURED_FRAME_H
