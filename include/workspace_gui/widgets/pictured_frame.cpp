#include "pictured_frame.h"

//-----------------------------------------------------------------------------
pictured_frame::pictured_frame(QWidget *parent_) :
    QFrame(parent_)
{
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void pictured_frame::paintEvent(QPaintEvent *ev)
{
    QPainter p( this );

    if ( !bg_image.isNull() )
        p.drawImage( QRect( 0,0, this->width(), this->height() ),
                     this->bg_image );

    ev->accept();
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void pictured_frame::set_bg_img(QImage img)
{
    this->bg_image = img;
    this->update();
}
//-----------------------------------------------------------------------------
