#include "actionprocessingwidget.h"
#include "ui_actionprocessingwidget.h"

#include "include/greencastlesingleton.h"

//-----------------------------------------------------------------------------
ActionProcessingWidget::ActionProcessingWidget(ACTION_INSTRUCTION_STRUCT instructions) :
    QWidget(0),
    ui(new Ui::ActionProcessingWidget)
{
    ui->setupUi(this);

    this->action_instruction = instructions;

    this->setup();
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
ActionProcessingWidget::ActionProcessingWidget(ACTION_INSTRUCTION_STRUCT::instruction_types instruction_type,
                                               QVariantList args) :
    QWidget(0),
    ui(new Ui::ActionProcessingWidget)
{
    ui->setupUi(this);

    ACTION_INSTRUCTION_STRUCT ais;
    ais.instruction_type = instruction_type;
    ais.args = args;

    this->action_instruction = ais;

    this->setup();
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void ActionProcessingWidget::setup()
{
    this->engagement_result = EXECUTE_ME_PLS;

    this->text_label_pointer = this->ui->label_description;

    if ( this->action_instruction.instruction_type == ACTION_INSTRUCTION_STRUCT::DOWNLOAD )
    {
        this->download_manager = new QNetworkAccessManager( this );

        connect( this->download_manager, SIGNAL(finished(QNetworkReply*)),
                 this, SLOT(download_finished(QNetworkReply*)) );

    }
    else
        this->download_manager = 0;

    this->init_sleeping_state();
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
ActionProcessingWidget::~ActionProcessingWidget()
{
    delete ui;
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void ActionProcessingWidget::init_sleeping_state()
{
    this->ui->progressBar->setMaximum(0);
    this->ui->progressBar->setMaximum(1);
    this->ui->toolButton_done->setVisible( false );
    this->ui->toolButton_restart->setVisible( false );
    this->ui->toolButton_pause->setVisible( false );
    this->ui->toolButton_stop->setVisible( false );
    this->setStyleSheet("");

    switch ( this->action_instruction.instruction_type )
    {
    //----------------------------------------
    case ACTION_INSTRUCTION_STRUCT::NONE:
    {
        // wat?
        this->ui->label_logo->setPixmap( QPixmap( ":/res/img/btn_help.png" ) );
        this->ui->label_description->setText( trUtf8("This App is going to crash.") );
    }
    break;
    //----------------------------------------
    case ACTION_INSTRUCTION_STRUCT::DOWNLOAD:
    {
        Q_ASSERT_X( this->action_instruction.args.count() == 2,
                    "ActionProcessingWidget::init_sleeping_state():",
                    "ACTION_INSTRUCTION _DOWNLOAD_ with args != 2" );

        this->ui->label_logo->setPixmap( QPixmap(":/res/img/btn_download.png") );
        this->ui->label_description->setText( trUtf8("Downloading File: %1")
                                              .arg( this->action_instruction.args[0].toString()
                                              .mid( this->action_instruction.args[0].toString().lastIndexOf("/")+1 ) ) );
    }
    break;
    //----------------------------------------
    case ACTION_INSTRUCTION_STRUCT::TOUCH:
    {
        Q_ASSERT_X( this->action_instruction.args.count() == 1,
                    "ActionProcessingWidget::init_sleeping_state():",
                    "ACTION_INSTRUCTION _TOUCH_ with args != 1" );

        this->ui->label_logo->setPixmap( QPixmap(":/res/img/btn_file.png") );
        this->ui->label_description->setText( trUtf8("Touching File: %1")
                                              .arg( this->action_instruction.args[0].toString()
                                              .mid( this->action_instruction.args[0].toString().lastIndexOf( QDir::separator() ) ) ) );
    }
    break;
    //----------------------------------------
    case ACTION_INSTRUCTION_STRUCT::COPY:
    {
        Q_ASSERT_X( this->action_instruction.args.count() == 2,
                    "ActionProcessingWidget::init_sleeping_state():",
                    "ACTION_INSTRUCTION _COPY_ with args != 2" );

        this->ui->label_logo->setPixmap( QPixmap(":/res/img/btn_copy.png") );
        this->ui->label_description->setText( trUtf8("Copy File: %1")
                                              .arg( this->action_instruction.args[0].toString()
                                              .mid( this->action_instruction.args[0].toString().lastIndexOf( QDir::separator() ) ) ) );
    }
    break;
    //----------------------------------------
    case ACTION_INSTRUCTION_STRUCT::COPY_EX:
    {
        Q_ASSERT_X( this->action_instruction.args.count() == 2,
                    "ActionProcessingWidget::init_sleeping_state():",
                    "ACTION_INSTRUCTION _COPY_EX_ with args != 2" );

        this->ui->label_logo->setPixmap( QPixmap(":/res/img/btn_copy.png") );
        this->ui->label_description->setText( trUtf8("Copy (External) File: %1")
                                              .arg( this->action_instruction.args[0].toString()
                                              .mid( this->action_instruction.args[0].toString().lastIndexOf( QDir::separator() ) ) ) );
    }
    break;
    //----------------------------------------
    case ACTION_INSTRUCTION_STRUCT::MOVE:
    {
        Q_ASSERT_X( this->action_instruction.args.count() == 2,
                    "ActionProcessingWidget::init_sleeping_state():",
                    "ACTION_INSTRUCTION _MOVE_ with args != 2" );

        this->ui->label_logo->setPixmap( QPixmap(":/res/img/btn_move.png") );
        this->ui->label_description->setText( trUtf8("Move File: %1")
                                              .arg( this->action_instruction.args[0].toString()
                                              .mid( this->action_instruction.args[0].toString().lastIndexOf( QDir::separator() ) ) ) );
    }
    break;
    //----------------------------------------
    case ACTION_INSTRUCTION_STRUCT::DELETE:
    {
        Q_ASSERT_X( this->action_instruction.args.count() == 1,
                    "ActionProcessingWidget::init_sleeping_state():",
                    "ACTION_INSTRUCTION _DELETE_ with args != 1" );

        this->ui->label_logo->setPixmap( QPixmap(":/res/img/close.png") );
        this->ui->label_description->setText( trUtf8("Delete File: %1")
                                              .arg( this->action_instruction.args[0].toString()
                                              .mid( this->action_instruction.args[0].toString().lastIndexOf( QDir::separator() ) ) ) );
    }
    break;
    //----------------------------------------
    case ACTION_INSTRUCTION_STRUCT::POPULATE:
    {
        Q_ASSERT_X( this->action_instruction.args.count() == 2,
                    "ActionProcessingWidget::init_sleeping_state():",
                    "ACTION_INSTRUCTION _POPULATE_ with args != 2" );

        this->ui->label_logo->setPixmap( QPixmap(":/res/img/btn_file.png") );
        this->ui->label_description->setText( trUtf8("Populate File: %1")
                                              .arg( this->action_instruction.args[0].toString()
                                              .mid( this->action_instruction.args[0].toString().lastIndexOf( QDir::separator() ) ) ) );
    }
    break;
    //----------------------------------------
    case ACTION_INSTRUCTION_STRUCT::UNZIP:
    {
        Q_ASSERT_X( this->action_instruction.args.count() == 2,
                    "ActionProcessingWidget::init_sleeping_state():",
                    "ACTION_INSTRUCTION _UNZIP_ with args != 2" );

        this->ui->label_logo->setPixmap( QPixmap(":/res/img/btn_zip.png") );
        this->ui->label_description->setText( trUtf8("Unzip File: %1")
                                              .arg( this->action_instruction.args[0].toString()
                                              .mid( this->action_instruction.args[0].toString().lastIndexOf( QDir::separator() ) ) ) );
    }
    break;
    //----------------------------------------
    case ACTION_INSTRUCTION_STRUCT::MKDIR:
    {
        Q_ASSERT_X( this->action_instruction.args.count() == 1,
                    "ActionProcessingWidget::init_sleeping_state():",
                    "ACTION_INSTRUCTION _MKDIR_ with args != 1" );

        this->ui->label_logo->setPixmap( QPixmap(":/res/img/btn_mkdir.png") );
        this->ui->label_description->setText( trUtf8("Creating directory: %1")
                                              .arg( this->action_instruction.args[0].toString()
                                              .mid( this->action_instruction.args[0].toString().lastIndexOf( QDir::separator() ) ) ) );
    }
    break;
    //----------------------------------------
    }
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void ActionProcessingWidget::init_error_state()
{
    this->ui->progressBar->setMaximum(0);
    this->ui->progressBar->setMaximum(1);
    this->ui->toolButton_done->setVisible( false );
    this->ui->toolButton_restart->setVisible( false );
    this->ui->toolButton_pause->setVisible( false );
    this->ui->toolButton_stop->setVisible( false );
    this->setStyleSheet( "QWidget#ActionProcessingWidget{ background-color: red; }"
                         "color: white;" );

    qDebug() << "ActionProcessingWidget::init_error_state(): ErrorMsg:" << this->error_msg;
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void ActionProcessingWidget::init_error_state(QString what_went_wrong)
{
    this->error_msg = what_went_wrong;
    this->init_error_state();
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void ActionProcessingWidget::init_error_state(QString what_went_wrong, QString what_to_tell_the_user)
{
    this->error_msg = what_went_wrong;
    this->init_error_state();

    emit this->failed( what_to_tell_the_user );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void ActionProcessingWidget::init_engage()
{
    switch ( this->action_instruction.instruction_type )
    {
    //----------------------------------------
    case ACTION_INSTRUCTION_STRUCT::NONE:
    {
        qDebug() << "ActionProcessingWidget::init_engage(): NONE: BOOM!";
        // wat?
        Q_ASSERT_X( 1 == 2,
                    "ActionProcessingWidget::init_sleeping_state():",
                    "ACTION_INSTRUCTION _NONE_ - this should not have happend at all.");
    }
    break;
    //----------------------------------------
    case ACTION_INSTRUCTION_STRUCT::DOWNLOAD:
    {
        qDebug() << "ActionProcessingWidget::init_engage(): DOWNLOADING: "  << this->action_instruction.args[0].toString();
        Q_ASSERT_X( this->action_instruction.args.count() == 2,
                    "ActionProcessingWidget::init_sleeping_state():",
                    "ACTION_INSTRUCTION _DOWNLOAD_ with args != 2" );

        QString remote_file_name = this->action_instruction.args[0].toString()
                                   .mid( this->action_instruction.args[0].toString().lastIndexOf( "/" ) + 1 );

        QFile f( gnurpgm.project_management->currentProject()->project_base_dir
                 + QDir::separator()
                 + this->action_instruction.args[1].toString() // "prefix" eg "res/music
                 + QDir::separator()
                 + remote_file_name );

        if ( f.open( QIODevice::WriteOnly  ) )
        {
            f.write( " " );
            f.close();

            // ok, let's start the download

            QNetworkReply *rep = this->download_manager->get( QNetworkRequest( QUrl( this->action_instruction.args[0].toString() ) ) );

            connect( rep, SIGNAL(downloadProgress(qint64,qint64)), this, SLOT(download_proceeds(qint64,qint64)) );
        }
        else
        {
            this->init_error_state( "ActionProcessingWidget::init_engage(): couldn't create (pre-download) file: " + f.fileName(),
                                    tr("The placeholder-file \"%1\" for your download \"%2\"  could not be created.")
                                    .arg( f.fileName()  )
                                    .arg( this->action_instruction.args[0].toString() ) );
        }
    }
    break;
    //----------------------------------------
    case ACTION_INSTRUCTION_STRUCT::TOUCH:
    {
        qDebug() << "ActionProcessingWidget::init_engage(): TOUCH: "  << this->action_instruction.args[0].toString();
        Q_ASSERT_X( this->action_instruction.args.count() == 1,
                    "ActionProcessingWidget::init_sleeping_state():",
                    "ACTION_INSTRUCTION _TOUCH_ with args != 1" );

        QFile f( this->action_instruction.args[0].toString() );

        if ( f.open( QIODevice::WriteOnly ) )
        {
            f.write( " " );
            f.close();

            init_succeeded_state();
        }
        else
            init_error_state( "ActionProcessingWidget::init_engage(): couldn't create (touch) file: " + f.fileName(),
                              tr("The file \"%1\" could not be touched.").arg( f.fileName() ) );
    }
    break;
    //----------------------------------------
    case ACTION_INSTRUCTION_STRUCT::MKDIR:
    {
        qDebug() << "ActionProcessingWidget::init_engage(): MKDIR: "  << this->action_instruction.args[0].toString();
        Q_ASSERT_X( this->action_instruction.args.count() == 1,
                    "ActionProcessingWidget::init_sleeping_state():",
                    "ACTION_INSTRUCTION _MKDIR_ with args != 1" );

        QDir d( this->action_instruction.args[0].toString() );


        if ( d.mkpath( this->action_instruction.args[0].toString() ) )
        {
            init_succeeded_state();
        }
        else
            init_error_state( "ActionProcessingWidget::init_engage(): couldn't create the folder: " + d.absolutePath(),
                              tr("The folder \"%1\" could not be created.").arg( d.absolutePath() ) );
    }
    break;
        //----------------------------------------
        case ACTION_INSTRUCTION_STRUCT::COPY:
        {
        qDebug() << "ActionProcessingWidget::init_engage(): COPY: "  << this->action_instruction.args[0].toString();
            Q_ASSERT_X( this->action_instruction.args.count() == 2,
                        "ActionProcessingWidget::init_sleeping_state():",
                        "ACTION_INSTRUCTION _COPY_ with args != 2" );

            QFile f;
            if ( f.copy( this->action_instruction.args[0].toString(),
                         this->action_instruction.args[1].toString() ) )
                init_succeeded_state();
            else
                init_error_state( "ActionProcessingWidget::init_engage(): couldn't copy file: " + f.fileName(),
                                  tr("The file \"%1\" could not be copied.").arg( f.fileName() ) );
        }
        break;
        //----------------------------------------
        case ACTION_INSTRUCTION_STRUCT::COPY_EX:
        {
            Q_ASSERT_X( this->action_instruction.args.count() == 2,
                        "ActionProcessingWidget::init_sleeping_state():",
                        "ACTION_INSTRUCTION _COPY_EX_ with args != 2" );

            QFile f;
            if ( f.copy( this->action_instruction.args[0].toString(),
                         this->action_instruction.args[1].toString() ) )
                init_succeeded_state();
            else
                init_error_state( "ActionProcessingWidget::init_engage(): couldn't copy ex file: " + f.fileName(),
                                  tr("The external file \"%1\" could not be copied.").arg( f.fileName() ) );
        }
        break;
    //----------------------------------------
    case ACTION_INSTRUCTION_STRUCT::MOVE:
    {
        Q_ASSERT_X( this->action_instruction.args.count() == 2,
                    "ActionProcessingWidget::init_sleeping_state():",
                    "ACTION_INSTRUCTION _MOVE_ with args != 2" );

        QFile f;
        if ( f.copy( this->action_instruction.args[0].toString(),
                     this->action_instruction.args[1].toString() ) )
        {
            f.setFileName( this->action_instruction.args[0].toString() );

            if ( f.remove() )
                init_succeeded_state();
            else
                init_error_state( "ActionProcessingWidget::init_engage(): couldn't delete (moving) file: " + f.fileName(),
                                  tr("The file \"%1\" could not be moved (but it has been copied).").arg( f.fileName() ) );
        }
        else
            init_error_state( "ActionProcessingWidget::init_engage(): couldn't copy (before moving) file: " + f.fileName(),
                              tr("The file \"%1\" could not be copied (before moving).").arg( f.fileName() ) );
    }
    break;
    //----------------------------------------
    case ACTION_INSTRUCTION_STRUCT::DELETE:
    {
        Q_ASSERT_X( this->action_instruction.args.count() == 1,
                    "ActionProcessingWidget::init_sleeping_state():",
                    "ACTION_INSTRUCTION _DELETE_ with args != 1" );

        QFile f;
        f.setFileName( this->action_instruction.args[0].toString() );

        if ( f.remove() )
            init_succeeded_state();
        else
            init_error_state( "ActionProcessingWidget::init_engage(): couldn't delete file: " + f.fileName(),
                              tr("The file \"%1\" could not be deleted.").arg( f.fileName() ) );
    }
    break;
    //----------------------------------------
    case ACTION_INSTRUCTION_STRUCT::POPULATE:
    {
        Q_ASSERT_X( this->action_instruction.args.count() == 2,
                    "ActionProcessingWidget::init_sleeping_state():",
                    "ACTION_INSTRUCTION _POPULATE_ with args != 2" );

        QFile f( this->action_instruction.args[0].toString() );

        if ( f.open( QIODevice::WriteOnly ) )
        {
            f.write( this->action_instruction.args[1].toByteArray() );
            f.close();

            init_succeeded_state();
        }
        else
            init_error_state( "ActionProcessingWidget::init_engage(): couldn't create file: " + f.fileName(),
                              tr("The file \"%1\" could not be created.").arg( f.fileName() ) );
    }
    break;
    //----------------------------------------
    case ACTION_INSTRUCTION_STRUCT::UNZIP:
    {/*
        Q_ASSERT_X( this->action_instruction.args.count() == 2,
                    "ActionProcessingWidget::init_sleeping_state():",
                    "ACTION_INSTRUCTION _UNZIP_ with args != 2" );

        // f's filename will be cnstructed like this:
        // "/path/to/my/project/" + "downloads/my_downloaded_zip.zip"
        QFile f_source_zip( gnurpgm.project_management->currentProject()->project_base_dir
                 + this->action_instruction.args[0].toString() );


        if ( f_source_zip.open( QIODevice::ReadOnly ) )
        {
            // filenames_and_data will contain the paths to all found files
            // and their binary, uncompressed data
            // like:
            // -> images/img1.png
            // -> images/more/img2.png
            // -> porn.jpeg

            // -------------------------------------------
            // 1) set the destination path where to unzip everything
            // -------------------------------------------

            // this will look something like:
            // "/path/to/my/project/" + "res/images"
            QString destination_base_path
                    = gnurpgm.project_management->currentProject()->project_base_dir
                     + this->action_instruction.args[1].toString()
                     + QDir::separator() ;

            // -------------------------------------------
            // 2) get the list of _all_ files and their contents of dat zip
            // -------------------------------------------

            QList< QPair<QString,QByteArray> > files_and_content;

            QByteArray arr = f_source_zip.readAll();

            QBuffer buff( &arr );
            buff.open( QIODevice::ReadOnly );

            QuaZip zipy( &buff );
            if ( zipy.open( QuaZip::mdUnzip ) )
            {
                QuaZipFile file(&zipy);

                for( bool f=zipy.goToFirstFile();
                     f;
                     f=zipy.goToNextFile())
                {
                    file.open(QIODevice::ReadOnly );

                    files_and_content.append( QPair<QString,QByteArray>(
                                                 file.getActualFileName(),
                                                 file.readAll() ) );

                    file.close();
                }

                zipy.close();
            }

            // -------------------------------------------
            // 3) create the extracted files on the local folder struct/harddrive
            // -------------------------------------------

            while ( !files_and_content.isEmpty() )
            {
                // check if the folder path exists, or if we'll have to create it
                QFileInfo info( destination_base_path + files_and_content.first().first );
                qDebug() << "checking path: " << info.absoluteDir().path();

                if ( !info.absoluteDir().exists() )
                {
                    qDebug() << "create path: " << info.absoluteDir().path();
                    info.absoluteDir().mkpath( info.absoluteDir().path() );
                }

                // write the actual, extracted file
                QFile f( info.absoluteFilePath() );
                if ( f.open( QIODevice::WriteOnly ) )
                {
                    qDebug() << "create file: " << info.absoluteFilePath();

                    f.write( files_and_content.first().second );

                    f.close();
                }
                else
                {
                    init_error_state( "ActionProcessingWidget::init_engage(): couldn't create file: " + info.absoluteFilePath(),
                                      tr("The file \"%1\" could not be created.").arg( f.fileName() ) );
                    return;
                }

                files_and_content.removeFirst();
            }

            f_source_zip.close();

            init_succeeded_state();
        }
        else
            init_error_state( "ActionProcessingWidget::init_engage(): couldn't read zip file: " + f_source_zip.fileName(),
                              tr("The zip-file \"%1\" could not be read.").arg( f_source_zip.fileName() ) );
    */}
    break;
    //----------------------------------------
    }
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void ActionProcessingWidget::init_succeeded_state()
{
    this->engagement_result = ENGAGE_SUCCEDED;
    this->ui->progressBar->setValue( ui->progressBar->maximum() );
    emit this->finished();
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void ActionProcessingWidget::on_toolButton_restart_clicked()
{
    // nah man
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void ActionProcessingWidget::on_toolButton_pause_clicked()
{
    // nah ... rly man
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void ActionProcessingWidget::on_toolButton_stop_clicked()
{

}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void ActionProcessingWidget::download_proceeds(qint64 received_bytes, qint64 max_bytes)
{
    this->ui->progressBar->setMaximum( max_bytes );
    this->ui->progressBar->setValue( received_bytes );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void ActionProcessingWidget::download_finished(QNetworkReply *rep)
{
    // trying to save the file to args[1] (QString)

    QString remote_file_name = this->action_instruction.args[0].toString()
                               .mid( this->action_instruction.args[0].toString().lastIndexOf( "/" ) + 1 );

    QFile f( gnurpgm.project_management->currentProject()->project_base_dir
             + QDir::separator()
             + this->action_instruction.args[1].toString() // "prefix" eg "res/music
             + QDir::separator()
             + remote_file_name );

    if ( f.open( QIODevice::WriteOnly  ) )
    {
        f.write( rep->readAll() );

        f.close();

        this->init_succeeded_state();
    }
    else
        this->init_error_state( "ActionProcessingWidget::init_engage(): couldn't create downloaded file: " + f.fileName(),
                                tr("The downloaded file \"%1\" could not be saved as \"%2\".")
                                .arg( remote_file_name )
                                .arg( f.fileName() ) );
}
//-----------------------------------------------------------------------------
