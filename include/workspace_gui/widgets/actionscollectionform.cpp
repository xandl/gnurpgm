#include "actionscollectionform.h"
#include "ui_actionscollectionform.h"

#include <QTimer>
#include <QMessageBox>
#include <QScriptEngine>

//-----------------------------------------------------------------------------
actionsCollectionForm::actionsCollectionForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::actionsCollectionForm)
{
    ui->setupUi(this);

    this->exec_state = PAUSED;
    this->processing_result = STILL_BUSY;

    this->ui->pushButton_showHide_errors->hide();
    this->ui->listWidget_errors->hide();
    this->ui->pushButton_cancel->setVisible( false );

}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
actionsCollectionForm::~actionsCollectionForm()
{
    delete ui;
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void actionsCollectionForm::on_pushButton_cancel_clicked()
{
    this->exec_state = PAUSED;

    if ( QMessageBox::question( 0,
                           tr("Really abort?"),
                           tr("Do you really want to abort this process?"),
                           QMessageBox::Yes,
                           QMessageBox::No )
         == QMessageBox::Yes )
    {
        this->processing_result = ABORTED;

        emit this->execution_aborted();

        return;
    }

    // else
    this->exec_state = EXECUTING;
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void actionsCollectionForm::add_action(ActionProcessingWidget::ACTION_INSTRUCTION_STRUCT new_action_data)
{
    this->action_process_widgets.append( new ActionProcessingWidget( new_action_data ) );
    connect( this->action_process_widgets.last(), SIGNAL(finished()), this, SLOT(a_process_finished()) );
    connect( this->action_process_widgets.last(), SIGNAL(failed(QString)), this, SLOT(a_process_failed(QString)) );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
QScriptValue actionsCollectionForm::add_touch_action(QString file_to_touch)
{
    ActionProcessingWidget::ACTION_INSTRUCTION_STRUCT new_action_data;
    new_action_data.instruction_type = ActionProcessingWidget::ACTION_INSTRUCTION_STRUCT::TOUCH;
    new_action_data.args.append( QVariant( project_base_path + file_to_touch ) );

    this->add_action( new_action_data );

    return QScriptValue("");
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
QScriptValue actionsCollectionForm::add_mkdir_action(QString relative_dir_path)
{
    ActionProcessingWidget::ACTION_INSTRUCTION_STRUCT new_action_data;
    new_action_data.instruction_type = ActionProcessingWidget::ACTION_INSTRUCTION_STRUCT::MKDIR;
    new_action_data.args.append( QVariant( project_base_path + relative_dir_path ) );

    this->add_action( new_action_data );

    return QScriptValue("");
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
QScriptValue actionsCollectionForm::add_download_action(QUrl file_to_download, QString local_file_destination)
{
    ActionProcessingWidget::ACTION_INSTRUCTION_STRUCT new_action_data;
    new_action_data.instruction_type = ActionProcessingWidget::ACTION_INSTRUCTION_STRUCT::TOUCH;
    new_action_data.args.append( QVariant( file_to_download.toString() ) );
    new_action_data.args.append( QVariant( project_base_path + local_file_destination ) );

    this->add_action( new_action_data );

    return QScriptValue("");
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
QScriptValue actionsCollectionForm::add_copy_action(QString file_to_copy, QString new_file_location)
{
    ActionProcessingWidget::ACTION_INSTRUCTION_STRUCT new_action_data;
    new_action_data.instruction_type = ActionProcessingWidget::ACTION_INSTRUCTION_STRUCT::COPY;
    new_action_data.args.append( QVariant( project_base_path + file_to_copy ) );
    new_action_data.args.append( QVariant( project_base_path + new_file_location ) );

    this->add_action( new_action_data );

    return QScriptValue("");
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
QScriptValue actionsCollectionForm::add_copy_from_ex_action(QString file_to_copy, QString new_file_location)
{
    ActionProcessingWidget::ACTION_INSTRUCTION_STRUCT new_action_data;
    new_action_data.instruction_type = ActionProcessingWidget::ACTION_INSTRUCTION_STRUCT::COPY;
    new_action_data.args.append( QVariant( file_to_copy ) );
    new_action_data.args.append( QVariant( project_base_path + new_file_location ) );

    this->add_action( new_action_data );

    return QScriptValue("");
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
QScriptValue actionsCollectionForm::add_move_action(QString file_to_move, QString new_file_location)
{
    ActionProcessingWidget::ACTION_INSTRUCTION_STRUCT new_action_data;
    new_action_data.instruction_type = ActionProcessingWidget::ACTION_INSTRUCTION_STRUCT::MOVE;
    new_action_data.args.append( QVariant( project_base_path + file_to_move ) );
    new_action_data.args.append( QVariant( project_base_path + new_file_location ) );

    this->add_action( new_action_data );

    return QScriptValue("");
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
QScriptValue actionsCollectionForm::add_delete_action(QString file_to_delete)
{
    ActionProcessingWidget::ACTION_INSTRUCTION_STRUCT new_action_data;
    new_action_data.instruction_type = ActionProcessingWidget::ACTION_INSTRUCTION_STRUCT::DELETE;
    new_action_data.args.append( QVariant( project_base_path + file_to_delete ) );

    this->add_action( new_action_data );

    return QScriptValue("");
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
QScriptValue actionsCollectionForm::add_populate_action(QString file_name, QString data_to_populate)
{
    ActionProcessingWidget::ACTION_INSTRUCTION_STRUCT new_action_data;
    new_action_data.instruction_type = ActionProcessingWidget::ACTION_INSTRUCTION_STRUCT::POPULATE;
    new_action_data.args.append( QVariant( project_base_path + file_name ) );
    new_action_data.args.append( QVariant( data_to_populate ) );

    this->add_action( new_action_data );

    return QScriptValue("");
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
QScriptValue actionsCollectionForm::add_unzip_action(QString file_to_unzip, QString local_folder_destination)
{
    ActionProcessingWidget::ACTION_INSTRUCTION_STRUCT new_action_data;
    new_action_data.instruction_type = ActionProcessingWidget::ACTION_INSTRUCTION_STRUCT::UNZIP;
    new_action_data.args.append( QVariant( project_base_path + file_to_unzip ) );
    new_action_data.args.append( QVariant( project_base_path + local_folder_destination ) );

    this->add_action( new_action_data );

    return QScriptValue("");
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void actionsCollectionForm::refresh_gui_list()
{
    // save all the widgets that are corrently nested into the listwidget
    for ( int i = 0; i < action_process_widgets.count(); i++ )
    {
        action_process_widgets.at(i)->setVisible( false );
        action_process_widgets.at(i)->setParent( 0 );
    }

    // now clear the empty listwidget items
    this->ui->listWidget_actions->clear();
    foreach ( ActionProcessingWidget* pap, this->action_process_widgets )
    {
        QListWidgetItem* it = new QListWidgetItem();
        it->setSizeHint( QSize( 50,50 ) );
        this->ui->listWidget_actions->addItem( it );
        this->ui->listWidget_actions->setItemWidget( it, pap );
        pap->setVisible( true );
    }
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void actionsCollectionForm::clear_action_widgets()
{
    while( !this->action_process_widgets.isEmpty() )
        delete this->action_process_widgets.takeFirst();

    this->ui->listWidget_errors->clear();

    this->refresh_gui_list();
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void actionsCollectionForm::clear()
{
    this->clear_action_widgets();
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void actionsCollectionForm::delete_action_widgets()
{
    this->clear_action_widgets();
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void actionsCollectionForm::process_next()
{
    // finding the next unfinished or failed step, to execute it
    for ( int i = 0; i < this->action_process_widgets.count(); i++ )
    {
        if ( this->action_process_widgets[i]->engagement_result == ActionProcessingWidget::EXECUTE_ME_PLS )
        {
            // ok, dis be our case
            this->ui->label_state->setText( tr("Processing: ") + this->action_process_widgets[i]->text_label_pointer->text() );
            this->ui->listWidget_actions->scrollToItem( ui->listWidget_actions->item( i ) );
            this->action_process_widgets[i]->init_engage();
            return;
        }
    }

    // we check'd 'em all and there weren't any waiting for their execution?
    // fine, we're done
    this->prepare_execution_finished();
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void actionsCollectionForm::prepare_execution_finished()
{
    this->ui->pushButton_start->setVisible( false );
    this->ui->pushButton_cancel->setVisible( false );

    this->ui->label_state->setText( tr("Finished!") );

    emit this->execution_finished();
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void actionsCollectionForm::on_pushButton_start_clicked()
{
    // reset all actions
    for ( int i = 0; i < this->action_process_widgets.count(); i++ )
        this->action_process_widgets[i]->init_sleeping_state();

    this->ui->pushButton_start->setVisible( false );
    this->ui->pushButton_cancel->setVisible( true );

    this->process_next();
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void actionsCollectionForm::a_process_finished()
{
    QTimer::singleShot( 100, this, SLOT(process_next()) );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void actionsCollectionForm::a_process_failed( QString error_msg_to_user )
{
    // stop?
    if ( QMessageBox::question(
             0,
             tr("Something went wrong..."),
             tr("The following error appeared:\n%1\nDo you want to ignore this error and continue?")
             .arg( error_msg_to_user ),
             QMessageBox::Ignore,
             QMessageBox::Cancel )
         == QMessageBox::Ignore )
    {
        this->process_next();
    }
    else
    {
        this->ui->pushButton_start->setVisible( true );
        this->ui->pushButton_cancel->setVisible( false );
    }
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void actionsCollectionForm::set_GIS(QString gnurpgm_install_script)
{
    this->clear();

    this->gnurpgm_installer_script = gnurpgm_install_script;

    this->interprete_gis_4_gui();
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void actionsCollectionForm::interprete_gis_4_gui()
{
    // Party hard!
    // GIS SCript ENGINE Setup

    QScriptEngine gis_script_engine;

    // let's seed an c++/Qt object into the script engine's pool
    QScriptValue js_alike_dom = gis_script_engine.newQObject(this, QScriptEngine::QtOwnership, QScriptEngine::ExcludeChildObjects
                                                    | QScriptEngine::ExcludeSuperClassMethods
                                                    | QScriptEngine::ExcludeSuperClassProperties );

    // Oh look!
    // The local actionsCollectionForm class ("this") is now known
    // to the Script Engine as "SirCameALot"
    gis_script_engine.globalObject().setProperty("SirCameALot",js_alike_dom);

    gis_script_engine.evaluate("function touch(relPath)"
                               "{return SirCameALot.add_touch_action(relPath);};");

    gis_script_engine.evaluate("function mkdir(relPath)"
                               "{return SirCameALot.add_mkdir_action(relPath);};");

    gis_script_engine.evaluate("function download(url,prefix)"
                               "{return SirCameALot.add_download_action(url,prefix);};");

    gis_script_engine.evaluate("function copy(relPath1,relPath2)"
                               "{return SirCameALot.add_copy_action(relPath1,relPath2);};");

    gis_script_engine.evaluate("function copy_from_ex(absolutePath,relPath)"
                               "{return SirCameALot.add_copy_from_ex_action(absolutePath,relPath);};");

    gis_script_engine.evaluate("function move(relPath1,relPath2)"
                               "{return SirCameALot.add_move_action(relPath1,relPath2);};");

    gis_script_engine.evaluate("function delete(relPath)"
                               "{return SirCameALot.add_delete_action(relPath);};");

    gis_script_engine.evaluate("function populate(relPath,data)"
                               "{return SirCameALot.add_populate_action(relPath,data);};");

    gis_script_engine.evaluate("function populate(relPath,isB64,data)"
                               "{return SirCameALot.add_populate_action(relPath,isB64,data);};");

    gis_script_engine.evaluate("function unzip(zipRelPath,prefix)"
                               "{return SirCameALot.add_unzip_action(zipRelPath,prefix);};");

    // this adds all the little progressbar things to the collection widget
    gis_script_engine.evaluate( this->gnurpgm_installer_script );

    // we're done. lol.

    if ( gis_script_engine.hasUncaughtException() )
    {
        QMessageBox::warning(
                    0,
                    tr("Warning"),
                    tr("There appeared errors in the GNURPGM Installer Script.\n"
                       "Some actions might have got lost during the parsing process, so "
                       "the result might be incomplete.\n\n"
                       "Proceed on your own risk, or contact the publisher of this script.\n"
                       "For more information, see the console output.") );
        qDebug() << "actionsCollectionForm::interprete_gis_4_gui(): UncaughtException at line"
                 << gis_script_engine.uncaughtExceptionLineNumber()
                 << ": " << gis_script_engine.uncaughtException().toString()
                 << endl
                 << "Here is it's backtrace: "
                 << gis_script_engine.uncaughtExceptionBacktrace().join("\n")
                 << "The whole script: "
                 << this->gnurpgm_installer_script;
    }

    this->refresh_gui_list();
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void actionsCollectionForm::set_project_base_path(QString absolute_basepath)
{
    if ( !absolute_basepath.endsWith( QDir::separator()  ) )
        absolute_basepath.append( QDir::separator() );

    this->project_base_path = absolute_basepath;
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
QString actionsCollectionForm::get_project_base_path()
{
    return this->project_base_path;
}
//-----------------------------------------------------------------------------
