#ifndef ACTIONSCOLLECTIONFORM_H
#define ACTIONSCOLLECTIONFORM_H

#include <QWidget>
#include <QUrl>
#include <QScriptValue>

#include "include/workspace_gui/widgets/actionprocessingwidget.h"

//-----------------------------------------------------------------------------
namespace Ui {
class actionsCollectionForm;
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
class actionsCollectionForm : public QWidget
{
    Q_OBJECT

public:
    explicit actionsCollectionForm(QWidget *parent = 0);
    ~actionsCollectionForm();

    enum EXEC_STATES
    {
        PAUSED = 0,
        EXECUTING = 1
    }
    exec_state;

    enum PROCESS_RESULT_SET
    {
        STILL_BUSY = 0,
        SUCCEDED = 1,
        ABORTED = 2,
        FAILED = 3
    }
    processing_result;

    QString gnurpgm_installer_script;

    void set_GIS( QString gnurpgm_install_script );

    void set_project_base_path( QString absolute_basepath );

    QString get_project_base_path();

private:

    QList<ActionProcessingWidget*> action_process_widgets;

    QString project_base_path;

private slots:

    void interprete_gis_4_gui();

    void process_next();

    void add_action( ActionProcessingWidget::ACTION_INSTRUCTION_STRUCT new_action_data );

    void refresh_gui_list();

    void clear_action_widgets();

    void clear(); // same as clear_action_widgets();

    void delete_action_widgets(); // same as clear_action_widgets();

public slots:

    void prepare_execution_finished();

    void a_process_finished();

    void a_process_failed(QString error_msg_to_user);

    // there here so ECMA parsing works

    QScriptValue add_touch_action( QString file_to_touch );

    QScriptValue add_mkdir_action( QString relative_dir_path );

    QScriptValue add_download_action( QUrl file_to_download, QString local_file_destination );

    QScriptValue add_copy_action( QString file_to_copy, QString new_file_location );

    QScriptValue add_copy_from_ex_action( QString file_to_copy, QString new_file_location );

    QScriptValue add_move_action( QString file_to_move, QString new_file_location );

    QScriptValue add_delete_action( QString file_to_delete );

    QScriptValue add_populate_action( QString file_name, QString data_to_populate );

    QScriptValue add_unzip_action( QString file_to_unzip, QString local_folder_destination );

    void on_pushButton_cancel_clicked();

    void on_pushButton_start_clicked();

private:
    Ui::actionsCollectionForm *ui;

signals:
    void execution_finished();

    void execution_failed();

    void execution_aborted();
};
//-----------------------------------------------------------------------------

#endif // ACTIONSCOLLECTIONFORM_H
