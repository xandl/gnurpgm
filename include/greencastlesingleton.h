#ifndef GREENCASTLESINGLETON_H
#define GREENCASTLESINGLETON_H

#include <QObject>


// Managers
#include "include/managers/projectmanagement.h"
#include "include/managers/errormanagement.h"

// Mainwindow
#include "include/mainwindow.h"

#define gnurpgm greenCastleSingleton::instance()
#define currentGnurpgmProject (greenCastleSingleton::instance().project_management->currentProject())
#define errorsManager (greenCastleSingleton::instance().error_management)

class greenCastleSingleton : public QObject
{
    Q_OBJECT

public:
    greenCastleSingleton();

    greenCastleSingleton(greenCastleSingleton const& copy);

    greenCastleSingleton& operator =(greenCastleSingleton const& copy);

    ~greenCastleSingleton();

    static greenCastleSingleton& instance();


    MainWindow* _mainwindow;

    projectManagement* project_management;

    errorManagement* error_management;

signals:

public slots:
    void setMainWindowPointer(MainWindow* mw);

    void initialize();

    void aboutGnurpgmDialog();

    void visitGnurpgmSfNet();

    void visitGnurpgmForums();

    void shutdown();

    QPair<bool,QString> _writeDataToFile(QString absoluteFilename, QByteArray data);

    QPair<bool,QString> _readDataFromFile(QString absoluteFilename, QByteArray* dataPointer);

    void setMainWindowToolbar(QList<QPair <bool,QAction*> > actions);

    void show_PreferencesDialogSheet();

private:
/*!
 * This method sets the menubar's actions up.
 * Actions related to open, create, clone projects will be shown.
*/
    void _setMainWindowMenu_to_preProjektMode();

/*!
 * This method sets the menubar's actions up.
 * Actions related to the current project (maps,...) will be shown.
*/
    void _setMainWindowMenu_to_projektMode();


};

#endif // GREENCASTLESINGLETON_H
