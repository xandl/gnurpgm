#include "_testingwelcomeform.h"
#include "ui__testingwelcomeform.h"

#include <QGraphicsDropShadowEffect>
#include <QFileDialog>
#include <QDesktopWidget>
#include "include/greencastlesingleton.h"

_testingWelcomeForm::_testingWelcomeForm(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::_testingWelcomeForm)
{
    ui->setupUi(this);

//    this->setup_toolbarBtns();

    QGraphicsDropShadowEffect* eff1 = new QGraphicsDropShadowEffect();
    eff1->setOffset(0,0);
    eff1->setBlurRadius( 8 );
    eff1->setColor( QColor( 66,66,66 ) );

    ui->listWidget_recentProjects->setGraphicsEffect( eff1 );

    QGraphicsDropShadowEffect* eff2 = new QGraphicsDropShadowEffect();
    eff2->setOffset(0,0);
    eff2->setBlurRadius( 12 );
    eff2->setColor( QColor( 66,66,66 ) );

    ui->frame_toolbar->setGraphicsEffect( eff2 );
}

_testingWelcomeForm::~_testingWelcomeForm()
{
    delete ui;
}

void _testingWelcomeForm::on_toolButton_resumePrevProject_clicked()
{
    this->resumePreviousProject_clicked();
}

void _testingWelcomeForm::on_toolButton_newProject_clicked()
{
    this->openNewProjectAssistant_clicked();
}

void _testingWelcomeForm::on_toolButton_openProject_clicked()
{
    this->openProjectFromFileDialog_clicked();
}

void _testingWelcomeForm::setup_toolbarBtns()
{
    QList<QPair <bool,QAction*> > toolbarBtns;

    toolbarBtns.append( QPair<bool,QAction*>(true,0));

    toolbarBtns.append( QPair<bool,QAction*>(false, new QAction( QIcon(":/icons/folder-recent.png"),
                                                                 tr("Resume Previous"),
                                                                 gnurpgm._mainwindow->toolbarActionGroup)) );

    toolbarBtns.append( QPair<bool,QAction*>(false, new QAction( QIcon(":/icons/folder-open.png"),
                                                                 tr("Open Project"),
                                                                 gnurpgm._mainwindow->toolbarActionGroup)) );

    toolbarBtns.append( QPair<bool,QAction*>(false, new QAction( QIcon(":/icons/folder_add.png"),
                                                                 tr("New Project"),
                                                                 gnurpgm._mainwindow->toolbarActionGroup)) );

    toolbarBtns.append( QPair<bool,QAction*>(false, new QAction( QIcon(":/icons/folder-download.png"),
                                                                 tr("Clone Project"),
                                                                 gnurpgm._mainwindow->toolbarActionGroup)) );

    toolbarBtns.append( QPair<bool,QAction*>(false, new QAction( QIcon(":/icons/network-wireless.png"),
                                                                 tr("Join (LAN)"),
                                                                 gnurpgm._mainwindow->toolbarActionGroup)) );

    toolbarBtns.append( QPair<bool,QAction*>(false, new QAction( QIcon(":/icons/applications-system.png"),
                                                                 tr("Preferences"),
                                                                 gnurpgm._mainwindow->toolbarActionGroup)) );

    toolbarBtns.append( QPair<bool,QAction*>(true,0));

//    toolbarBtns[1].second->setCheckable(true);
//    toolbarBtns[2].second->setCheckable(true);
//    toolbarBtns[3].second->setCheckable(true);
//    toolbarBtns[4].second->setCheckable(true);
//    toolbarBtns[5].second->setCheckable(true);
//    toolbarBtns[6].second->setCheckable(true);

    connect( toolbarBtns[1].second, SIGNAL(triggered()),
             this, SLOT(on_toolButton_resumePrevProject_clicked()) );

    connect( toolbarBtns[2].second, SIGNAL(triggered()),
             this, SLOT(openProjectFromFileDialog_clicked()) );

    connect( toolbarBtns[3].second, SIGNAL(triggered()),
             this, SLOT(openNewProjectAssistant_clicked()) );

    connect( toolbarBtns[4].second, SIGNAL(triggered()),
             this, SLOT(cloneProject_clicked()) );

    connect( toolbarBtns[5].second, SIGNAL(triggered()),
             this, SLOT(joinLan_clicked()) );

    connect( toolbarBtns[6].second, SIGNAL(triggered()),
             this, SLOT(showPreferences_clicked()) );

    gnurpgm.setMainWindowToolbar( toolbarBtns );
}

void _testingWelcomeForm::resumePreviousProject_clicked()
{
    emit this->resumeLastProjectPlease();
}

void _testingWelcomeForm::openProjectFromFileDialog_clicked()
{
    QString path = "";
    QFileDialog* dg = new QFileDialog( 0,
                                        tr("Open an project"),
                                        QDir::homePath(),
                                        tr("*.gnurpgm (GNURPGM Project)") );

#ifdef Q_OS_ANDROID
    dg->setWindowModality(Qt::ApplicationModal);
    dg->setGeometry(QApplication::desktop()->availableGeometry());
#endif
#ifdef Q_OS_MAC
    dg->setWindowFlags( Qt::Sheet );
    dg->setWindowModality(Qt::WindowModal);
    dg->setParent( this->parentWidget() );
#endif

    if (dg->exec() == QDialog::Accepted)
    {
        if (!dg->selectedFiles().isEmpty())
            path = dg->selectedFiles().first();
    }

    delete dg;

    if (!path.isEmpty())
        emit this->openProjectPlease(path);
}

void _testingWelcomeForm::openNewProjectAssistant_clicked()
{
//    emit this->createNewProjectPlease();
    gnurpgm._mainwindow->gotoForm( MainWindow::NEW_PROJECT_ASSISTANT );
}

void _testingWelcomeForm::cloneProject_clicked()
{
    gnurpgm._mainwindow->gotoForm( MainWindow::WORKSPACE );
}

void _testingWelcomeForm::joinLan_clicked()
{

}

void _testingWelcomeForm::showPreferences_clicked()
{
    gnurpgm.show_PreferencesDialogSheet();
}
