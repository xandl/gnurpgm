#ifndef _TESTINGWELCOMEFORM_H
#define _TESTINGWELCOMEFORM_H

#include <QFrame>

namespace Ui {
class _testingWelcomeForm;
}

class _testingWelcomeForm : public QFrame
{
    Q_OBJECT

public:
    explicit _testingWelcomeForm(QWidget *parent = 0);
    ~_testingWelcomeForm();

    void setup_toolbarBtns();

public slots:
    void resumePreviousProject_clicked();

    void openProjectFromFileDialog_clicked();

    void openNewProjectAssistant_clicked();

    void cloneProject_clicked();

    void joinLan_clicked();

    void showPreferences_clicked();

private slots:
    void on_toolButton_resumePrevProject_clicked();

    void on_toolButton_newProject_clicked();

    void on_toolButton_openProject_clicked();

signals:
    void resumeLastProjectPlease();

    void openProjectPlease(QString absoluteProjectFilePath);

    void createNewProjectPlease();

private:
    Ui::_testingWelcomeForm *ui;
};

#endif // _TESTINGWELCOMEFORM_H
