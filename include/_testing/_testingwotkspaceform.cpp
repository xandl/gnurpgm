#include "_testingwotkspaceform.h"
#include "ui__testingwotkspaceform.h"

#include <QAction>
#include "include/greencastlesingleton.h"

//---------------------------------------------------------------------------
_testingWotkspaceForm::_testingWotkspaceForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::_testingWotkspaceForm)
{
    ui->setupUi(this);
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
_testingWotkspaceForm::~_testingWotkspaceForm()
{
    delete ui;
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void _testingWotkspaceForm::setup_toolbarBtns()
{
    QList<QPair <bool,QAction*> > toolbarBtns;

    toolbarBtns.append( QPair<bool,QAction*>(true,0));

    toolbarBtns.append( QPair<bool,QAction*>(false, new QAction( QIcon(":/icons/switchTo.png"),
                                                                 tr("Switch to .."),
                                                                 gnurpgm._mainwindow->toolbarActionGroup)) );

    toolbarBtns.append( QPair<bool,QAction*>(false, new QAction( QIcon(":/icons/maps.png"),
                                                                 tr("Maps"),
                                                                 gnurpgm._mainwindow->toolbarActionGroup)) );

    toolbarBtns.append( QPair<bool,QAction*>(false, new QAction( QIcon(":/icons/matrix.png"),
                                                                 tr("Matrix"),
                                                                 gnurpgm._mainwindow->toolbarActionGroup)) );

    toolbarBtns.append( QPair<bool,QAction*>(false, new QAction( QIcon(":/icons/resources.png"),
                                                                 tr("Resources"),
                                                                 gnurpgm._mainwindow->toolbarActionGroup)) );

    toolbarBtns.append( QPair<bool,QAction*>(false, new QAction( QIcon(":/icons/tools.png"),
                                                                 tr("Tools"),
                                                                 gnurpgm._mainwindow->toolbarActionGroup)) );

    toolbarBtns.append( QPair<bool,QAction*>(false, new QAction( QIcon(":/icons/preferences-desktop.png"),
                                                                 tr("Details"),
                                                                 gnurpgm._mainwindow->toolbarActionGroup)) );

    toolbarBtns.append( QPair<bool,QAction*>(false, new QAction( QIcon(":/icons/play.png"),
                                                                 tr("Start"),
                                                                 gnurpgm._mainwindow->toolbarActionGroup)) );

    toolbarBtns.append( QPair<bool,QAction*>(false, new QAction( QIcon(":/icons/applications-system.png"),
                                                                 tr("Preferences"),
                                                                 gnurpgm._mainwindow->toolbarActionGroup)) );

    toolbarBtns.append( QPair<bool,QAction*>(true,0));

    connect( toolbarBtns[8].second, SIGNAL(triggered()),
            this, SLOT(showPreferencesPage()));

    gnurpgm.setMainWindowToolbar( toolbarBtns );
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void _testingWotkspaceForm::showPreferencesPage()
{
    gnurpgm.show_PreferencesDialogSheet();
}
//---------------------------------------------------------------------------
