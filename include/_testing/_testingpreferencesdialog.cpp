#include "_testingpreferencesdialog.h"
#include "ui__testingpreferencesdialog.h"

#include <QActionGroup>
#include <QDebug>
#include "include/greencastlesingleton.h"

//---------------------------------------------------------------------------
_testingPreferencesDialog::_testingPreferencesDialog(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::_testingPreferencesDialog)
{
    ui->setupUi(this);
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
_testingPreferencesDialog::~_testingPreferencesDialog()
{
    delete ui;
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void _testingPreferencesDialog::setup_toolbarBtns()
{
    QList<QPair <bool,QAction*> > toolbarBtns;

    toolbarBtns.append( QPair<bool,QAction*>(true,0));

    if ( gnurpgm.project_management->currentProject() == 0 )
    {
        toolbarBtns.append( QPair<bool,QAction*>(false, new QAction( QIcon(":/icons/package-install.png"),
                                                                     tr("Back to welcome screen"),
                                                                     gnurpgm._mainwindow->toolbarActionGroup)) );

        connect( toolbarBtns[1].second, SIGNAL(triggered()),
                 this, SLOT(backToWelcome_clicked()) );
    }
    else
    {
        toolbarBtns.append( QPair<bool,QAction*>(false, new QAction( QIcon(":/icons/package-install.png"),
                                                                     tr("Back to workspace"),
                                                                     gnurpgm._mainwindow->toolbarActionGroup)) );

        connect( toolbarBtns[1].second, SIGNAL(triggered()),
                 this, SLOT(backToWorkspace_clicked()) );
    }

    toolbarBtns.append( QPair<bool,QAction*>(false, new QAction( QIcon(":/icons/config-users.png"),
                                                                 tr("Social"),
                                                                 gnurpgm._mainwindow->toolbarActionGroup)) );

    toolbarBtns.append( QPair<bool,QAction*>(false, new QAction( QIcon(":/icons/user-desktop.png"),
                                                                 tr("Appearance"),
                                                                 gnurpgm._mainwindow->toolbarActionGroup)) );

    toolbarBtns.append( QPair<bool,QAction*>(false, new QAction( QIcon(":/icons/preferences-system-firewall.png"),
                                                                 tr("Security"),
                                                                 gnurpgm._mainwindow->toolbarActionGroup)) );

    toolbarBtns.append( QPair<bool,QAction*>(false, new QAction( QIcon(":/icons/gnome-nettool.png"),
                                                                 tr("Connectivity"),
                                                                 gnurpgm._mainwindow->toolbarActionGroup)) );

    toolbarBtns.append( QPair<bool,QAction*>(false, new QAction( QIcon(":/icons/preferences-desktop-accessibility.png"),
                                                                 tr("Accessibility"),
                                                                 gnurpgm._mainwindow->toolbarActionGroup)) );

    toolbarBtns.append( QPair<bool,QAction*>(false, new QAction( QIcon(":/icons/preferences-desktop-locale.png"),
                                                                 tr("Language"),
                                                                 gnurpgm._mainwindow->toolbarActionGroup)) );

    toolbarBtns.append( QPair<bool,QAction*>(true,0));

    toolbarBtns[2].second->setCheckable(true);
    toolbarBtns[3].second->setCheckable(true);
    toolbarBtns[4].second->setCheckable(true);
    toolbarBtns[5].second->setCheckable(true);
    toolbarBtns[6].second->setCheckable(true);
    toolbarBtns[7].second->setCheckable(true);

    connect( toolbarBtns[2].second, SIGNAL(triggered()),
             this, SLOT(social_clicked()) );

    connect( toolbarBtns[3].second, SIGNAL(triggered()),
             this, SLOT(appearance_clicked()) );

    connect( toolbarBtns[4].second, SIGNAL(triggered()),
             this, SLOT(security_clicked()) );

    connect( toolbarBtns[5].second, SIGNAL(triggered()),
             this, SLOT(connectivity_clicked()) );

    connect( toolbarBtns[6].second, SIGNAL(triggered()),
             this, SLOT(accessibility_clicked()) );

    connect( toolbarBtns[7].second, SIGNAL(triggered()),
             this, SLOT(language_clicked()) );


    toolbarBtns[2].second->setChecked(true);
    social_clicked();

    gnurpgm.setMainWindowToolbar( toolbarBtns );
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void _testingPreferencesDialog::backToWelcome_clicked()
{
    this->save_and_apply();
    gnurpgm._mainwindow->gotoForm( MainWindow::WELCOME_PAGE );
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void _testingPreferencesDialog::backToWorkspace_clicked()
{
    this->save_and_apply();
    gnurpgm._mainwindow->gotoForm( MainWindow::WORKSPACE );
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void _testingPreferencesDialog::social_clicked()
{
    ui->stackedWidget->slideInIdx( ui->stackedWidget->indexOf( ui->page_social ) );
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void _testingPreferencesDialog::appearance_clicked()
{
    ui->stackedWidget->slideInIdx( ui->stackedWidget->indexOf( ui->page_appearance ) );
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void _testingPreferencesDialog::security_clicked()
{
    ui->stackedWidget->slideInIdx( ui->stackedWidget->indexOf( ui->page_security ) );
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void _testingPreferencesDialog::connectivity_clicked()
{
    ui->stackedWidget->slideInIdx( ui->stackedWidget->indexOf( ui->page_connectivity ) );
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void _testingPreferencesDialog::accessibility_clicked()
{
    ui->stackedWidget->slideInIdx( ui->stackedWidget->indexOf( ui->page_accessability ) );
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void _testingPreferencesDialog::language_clicked()
{
    ui->stackedWidget->slideInIdx( ui->stackedWidget->indexOf( ui->page_language) );
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void _testingPreferencesDialog::save_and_apply()
{
    // todo ._.
}
//---------------------------------------------------------------------------

