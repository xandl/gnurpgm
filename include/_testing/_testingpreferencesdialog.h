#ifndef _TESTINGPREFERENCESDIALOG_H
#define _TESTINGPREFERENCESDIALOG_H

#include <QFrame>

//---------------------------------------------------------------------------
namespace Ui {
class _testingPreferencesDialog;
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
class _testingPreferencesDialog : public QFrame
{
    Q_OBJECT

public:
    explicit _testingPreferencesDialog(QWidget *parent = 0);
    ~_testingPreferencesDialog();

    void setup_toolbarBtns();

public slots:

    void backToWelcome_clicked();

    void backToWorkspace_clicked();

    void social_clicked();

    void appearance_clicked();

    void security_clicked();

    void connectivity_clicked();

    void accessibility_clicked();

    void language_clicked();

private slots:
    void save_and_apply();

private:
    Ui::_testingPreferencesDialog *ui;
};
//---------------------------------------------------------------------------

#endif // _TESTINGPREFERENCESDIALOG_H
