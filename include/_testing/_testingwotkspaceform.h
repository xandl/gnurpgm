#ifndef _TESTINGWOTKSPACEFORM_H
#define _TESTINGWOTKSPACEFORM_H

#include <QWidget>

namespace Ui {
class _testingWotkspaceForm;
}

class _testingWotkspaceForm : public QWidget
{
    Q_OBJECT

public:
    explicit _testingWotkspaceForm(QWidget *parent = 0);
    ~_testingWotkspaceForm();

    void setup_toolbarBtns();

public slots:
    void showPreferencesPage();

private:
    Ui::_testingWotkspaceForm *ui;
};

#endif // _TESTINGWOTKSPACEFORM_H
