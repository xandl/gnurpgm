#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QActionGroup>
//#include <QQuickView>
#include "include/_testing/_testingwotkspaceform.h"
#include "include/_testing/_testingwelcomeform.h"
#include "include/_testing/_testingpreferencesdialog.h"
#include "include/new_project_form.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

//    QQuickView* _qml_view_pointer;
//    QWidget* _qml_view_widget;
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QToolBar* toolbar();

    QActionGroup* toolbarActionGroup;

    _testingWelcomeForm* welcomeForm;

    new_project_Form* newProjectAssistant;

    _testingWotkspaceForm* workspaceForm;

    _testingPreferencesDialog* preferencesForm;

    enum mainForms
    {
        WELCOME_PAGE,
        NEW_PROJECT_ASSISTANT,
        LOADING_SCREEN,
        WORKSPACE,
        PREFERENCES
    };

    void gotoForm(mainForms targetForm);

public slots:
    void newProAssistant_createdNewProject_soWeOpenItNow(QString absolutPathToProjectFile);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
