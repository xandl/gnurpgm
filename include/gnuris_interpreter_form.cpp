#include "gnuris_interpreter_form.h"
#include "ui_gnuris_interpreter_form.h"

#include <QMessageBox>

gnuris_interpreter_Form::gnuris_interpreter_Form(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::gnuris_interpreter_Form)
{
    ui->setupUi(this);
}

gnuris_interpreter_Form::~gnuris_interpreter_Form()
{
    delete ui;
}

void gnuris_interpreter_Form::on_toolButton_1_custom_path_clicked()
{
    QString path = QFileDialog::getExistingDirectory();

    if ( path.isEmpty() )
        return;

    ui->lineEdit_1_custom_path->setText( path );
}

void gnuris_interpreter_Form::on_toolButton_2_gis_clicked()
{
    QString path = QFileDialog::getOpenFileName( 0, tr("SELECT YOUR .GIS FILE PLS"), QString(), "GnurpgmInstallerSkript (*.gis)" );

    if ( path.isEmpty() )
        return;

    ui->lineEdit_2_gis->setText( path );
}

void gnuris_interpreter_Form::on_pushButton_load_gis_clicked()
{
    QFile f( ui->lineEdit_2_gis->text() );

    if ( f.open( QIODevice::ReadOnly ) )
    {
        this->ui->widget->set_project_base_path( ui->lineEdit_1_custom_path->text() );

        this->ui->widget->set_GIS( f.readAll() );
        f.close();

    }
    else
        QMessageBox::critical( 0, tr("Error"), tr("Couldn't open/read GIS file.") );
}
