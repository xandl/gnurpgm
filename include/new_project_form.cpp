#include "new_project_form.h"
#include "ui_new_project_form.h"

#include <QTimer>
#include <QDesktopServices>
#include "include/greencastlesingleton.h"

//-----------------------------------------------------------------------------
new_project_Form::new_project_Form(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::new_project_Form)
{
    ui->setupUi(this);

    this->ui->label_name_usb_shit_description->setVisible( false );
    this->ui->checkBox_name_make_it_fat32->setVisible( false );

    this->restart();
    this->project_logo_absolute_path = ":/img/gnurpgm_logo.png";

    connect( this->ui->widget_execution_actionsCollection, SIGNAL(execution_finished()),
             this, SLOT(on_pushButton_next_clicked()) );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
QStringList new_project_Form::get_new_project_basic_fileslist()
{
    QStringList files;
    files << "project.gnurpgm";
    files << "logo.png";

    return files;
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
QString new_project_Form::get_new_project_absolute_path()
{
    return this->ui->label_location_final_path->text()
            .mid( 0,
                  this->ui->label_location_final_path->text().
                  lastIndexOf( QDir::separator() ) );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
new_project_Form::~new_project_Form()
{
    delete ui;
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void new_project_Form::on_pushButton_next_clicked()
{
    int step = this->ui->stackedWidget->currentIndex();

    //  0 = welcome
    //  1 = project name
    //  2 = logo
    //  3 = project path
    //  4 = migration
    //  5 = resources
    //  6 = summary
    //  7 = execution
    //  8 = execution::migration
    //  9 = execution::downloads
    // 10 = done

    // checking current step
    // what is the next step after the primary project data das been
    // created?
    if ( step == 7 )
    {
        // we dont want to migrate from somethig
        // we dont waht to download any resources
        // -> done
        if ( ui->radioButton_migrate_nothing->isChecked()
             && ui->radioButton_resources_nothing->isChecked() )
            step = 10; // -> goto step 10 == done

        else
        {
            // we _do_ want to migrate from somethig
            if ( !ui->radioButton_migrate_nothing->isChecked() )
                step += 1;

            // we dont want to migrate from somethig
            // we _do_ want to download any resources
            else if ( !ui->radioButton_resources_nothing->isChecked() )
                step += 2;

        }
    }
    // migration done - start downloads now?
    else if ( step == 8 )
    {
        // -> done
        if ( ui->radioButton_resources_nothing->isChecked() )
            step += 2;

        // we _do_ want to download any resources
        else
            step += 1;
    }

    // done -> open workspace now
    else if( step == 10 )
    {
        this->restart();
        emit this->quit_me();
        emit this->new_project_created( this->get_new_project_absolute_path()
                                        + QDir::separator()
                                        + "project.gnurpgm" );
//        system_sync_hub::instanz().archivements->unlock_feature( 9000 );
    }

    else
        step ++;


    this->ui->stackedWidget->slideInIdx( step );

    this->make_current_step_bold( step );

    this->apply_logic_for_step( step );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void new_project_Form::on_pushButton_previous_clicked()
{
    this->ui->stackedWidget->slideInPrev();

    int step = ((this->ui->stackedWidget->currentIndex() == 0)
                ? 0
                : (this->ui->stackedWidget->currentIndex() -1) );

    this->make_current_step_bold( step );

    this->apply_logic_for_step( step );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void new_project_Form::on_pushButton_abort_clicked()
{
    emit this->quit_me();
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void new_project_Form::make_current_step_bold( int step )
{
    QFont f_norm( "Ubuntu", -1, QFont::Normal );
    QFont f_bold( "Ubuntu", -1, QFont::Bold );

    // unbold all
    this->ui->label_step1->setFont( f_norm );
    this->ui->label_step2->setFont( f_norm );
    this->ui->label_step3->setFont( f_norm );
    this->ui->label_step4->setFont( f_norm );
    this->ui->label_step5->setFont( f_norm );
    this->ui->label_step5->setFont( f_norm );
    this->ui->label_step6->setFont( f_norm );
    this->ui->label_step7->setFont( f_norm );
    this->ui->label_step8->setFont( f_norm );
    this->ui->label_step81->setFont( f_norm );
    this->ui->label_step82->setFont( f_norm );
    this->ui->label_step9->setFont( f_norm );

    // make current step bold
    switch ( step )
    {

    case 0: // welcome
        this->ui->label_step1->setFont( f_bold );
    break;

    case 1: // name
        this->ui->label_step2->setFont( f_bold );
    break;

    case 2: // logo
        this->ui->label_step3->setFont( f_bold );
    break;

    case 3: // location
        this->ui->label_step4->setFont( f_bold );
    break;

    case 4: // migration
        this->ui->label_step5->setFont( f_bold );
    break;

    case 5: // ressources
        this->ui->label_step6->setFont( f_bold );
    break;

    case 6: // summary
        this->ui->label_step7->setFont( f_bold );
    break;

    case 7: // execution
        this->ui->label_step8->setFont( f_bold );
    break;

    case 8: // execution::migration
        this->ui->label_step81->setFont( f_bold );
    break;

    case 9: // execution::downloads
        this->ui->label_step82->setFont( f_bold );
    break;

    case 10: // done
        this->ui->label_step9->setFont( f_bold );
    break;

    }
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void new_project_Form::restart()
{
    this->ui->stackedWidget->slideInIdx( 0 );

    this->make_current_step_bold( 0 );

    this->ui->widget_ghost_warning->setVisible( (((qrand() % 12) == 0) ? true : false ) );

    this->ui->pushButton_next->setText( tr("Next") );

    this->ui->pushButton_previous->setEnabled( false );

    this->ui->pushButton_next->setEnabled( false );

    // reset names stuff
    this->ui->lineEdit_name_dirty->clear();

    this->ui->label_name_clean->setVisible( false );

    this->ui->lineEdit_name_clean->setVisible( false );

    // reset logo stuff
    this->ui->pushButton_pick_logo->setIcon( QIcon(":/img/gnurpgm_logo.png") );

    // reset location stuff
    this->ui->lineEdit_location_folder->clear();

    // reset migration stuff
    this->ui->radioButton_migrate_nothing->setChecked( true );

    // reset resource dealing stuff
    this->ui->radioButton_resources_nothing->setChecked( true );


    this->apply_logic_for_step( 0 );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void new_project_Form::apply_logic_for_step(int step)
{
    this->ui->pushButton_next->setEnabled( false );

    // make current step bold
    switch ( step )
    {

    case 0: // welcome
        this->ui->pushButton_previous->setEnabled( false );
        this->ui->pushButton_next->setEnabled( true );
    break;

    case 1: // name
        // waiting for the uzer to type something into dat dirty name lineedit
        this->ui->pushButton_previous->setEnabled( true );
        if ( this->ui->lineEdit_name_dirty->text().length() >= 3 )
            this->ui->pushButton_next->setEnabled( true );
    break;

    case 2: // logo
        this->ui->pushButton_previous->setEnabled( true );
        this->ui->pushButton_next->setEnabled( true );
    break;

    case 3: // location
        this->ui->pushButton_previous->setEnabled( true );
        if ( !ui->lineEdit_location_folder->text().isEmpty() )
            this->ui->pushButton_next->setEnabled( true );
    break;

    case 4: // migration
        this->ui->pushButton_previous->setEnabled( true );
        this->ui->pushButton_next->setEnabled( true );
    break;

    case 5: // ressources
        this->ui->pushButton_previous->setEnabled( true );
        this->ui->pushButton_next->setEnabled( true );
    break;

    case 6: // summary
        this->ui->pushButton_previous->setEnabled( true );
        this->ui->pushButton_next->setEnabled( true );

        this->ui->listWidget_summary->clear();
        this->ui->listWidget_summary->addItem( new QListWidgetItem(
                                                   QIcon( ":/img/btn_arrow_right.png" ),
                                                   tr("Deploying project files to: %1")
                                                   .arg( this->get_new_project_absolute_path() ) ) );
        this->ui->listWidget_summary->addItem( new QListWidgetItem(
                                                   QIcon( ":/img/btn_arrow_right.png" ),
                                                   tr("The following files will be created:") ) );

        foreach ( QString filename, this->get_new_project_basic_fileslist() )
            this->ui->listWidget_summary->addItem( new QListWidgetItem(
                                                       QIcon( ":/img/btn_add.png" ),
                                                       filename ) );

        // ignoring migration and ressources at this time point -> TODO:

    break;

    case 7: // execution
        this->ui->pushButton_previous->setEnabled( false );
        this->ui->pushButton_next->setEnabled( false );
        this->execution_start();
    break;

    case 8: // execution::migration
        this->ui->pushButton_previous->setEnabled( false );
        this->ui->pushButton_next->setEnabled( false );
        this->execution_migration_start();
    break;

    case 9: // execution::downloads
        this->ui->pushButton_previous->setEnabled( false );
        this->ui->pushButton_next->setEnabled( false );
        this->execution_downloads_start();
    break;

    case 10: // done
        this->ui->pushButton_previous->setEnabled( false );
        this->ui->pushButton_next->setEnabled( true );

        this->ui->pushButton_next->setText( tr("Done") );
    break;
    }

    this->ui->label_step81->setEnabled( !ui->radioButton_migrate_nothing->isChecked() );

    this->ui->label_step82->setEnabled( !ui->radioButton_resources_nothing->isChecked() );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void new_project_Form::execution_start()
{
    this->ui->widget_execution_actionsCollection->set_project_base_path( get_project_base_path() );
    this->ui->widget_execution_actionsCollection->set_GIS( get_gis_from_summary() );
    QTimer::singleShot( 100, this->ui->widget_execution_actionsCollection, SLOT(on_pushButton_start_clicked()) );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void new_project_Form::execution_migration_start()
{}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void new_project_Form::execution_downloads_start()
{}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void new_project_Form::on_lineEdit_name_dirty_textChanged(const QString &arg1)
{
    if ( ui->checkBox_name_make_it_fat32->isChecked() )
        this->ui->lineEdit_name_clean->setText( QString( arg1 ).remove("!").remove("*")
                                                .remove("\"").remove("\r").remove("\t")
                                                .remove("\n").remove(":").remove("/")
                                                .remove("\\").remove("?").remove("'")
                                                .remove("%").remove("$").remove("§")
                                                .remove("|").simplified() );

    if ( ui->checkBox_name_make_it_fat32->isChecked() )
        this->ui->pushButton_next->setEnabled( ((this->ui->lineEdit_name_clean->text().length() >= 3 ) ? true : false ) );

    else
        this->ui->pushButton_next->setEnabled( ((arg1.length() >= 3 ) ? true : false ) );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void new_project_Form::on_checkBox_name_make_it_fat32_toggled(bool checked)
{
    Q_UNUSED( checked );

    this->ui->label_name_clean->setVisible( ui->checkBox_name_make_it_fat32->isChecked() );
    this->ui->lineEdit_name_clean->setVisible( ui->checkBox_name_make_it_fat32->isChecked() );

    this->on_lineEdit_name_dirty_textChanged( QString( ui->lineEdit_name_dirty->text() ) );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void new_project_Form::on_pushButton_pick_logo_clicked()
{
    QString path = QFileDialog::getOpenFileName(
                0,
                tr("Pick a logo for your project"),
                QDir::homePath(),
                tr("PNG (*.png);;Images (*.png *.jpeg *.jpg *.bmp *.tga *.ico") );

    if ( path.isEmpty() )
        return;

    QImage new_logo;

    if ( new_logo.load( path ) )
    {
        project_logo_absolute_path = path;

        if ( new_logo.width() > 128
             || new_logo.height() > 128 )
            new_logo = new_logo.scaled( 128, 128, Qt::KeepAspectRatio, Qt::SmoothTransformation );

        this->ui->pushButton_pick_logo->setIcon( QIcon( QPixmap::fromImage( new_logo ) ) );
    }
    else
    {
        this->project_logo_absolute_path = ":/img/gnurpgm_logo.png";
        this->ui->pushButton_pick_logo->setIcon( QIcon( ":/img/gnurpgm_logo.png" ) );
    }

}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void new_project_Form::on_pushButton_pick_location_folder_clicked()
{
    QString path = QFileDialog::getExistingDirectory(
                0,
                tr("Where to place the project folder"),
                QDir::homePath() );

    if ( path.isEmpty() )
        return;

    QString minimal_clean_name = ui->lineEdit_name_dirty->text()
            .simplified().remove("*").remove("/")
            .remove("\\").remove("|");

    this->ui->lineEdit_location_folder->setText( path );
    this->ui->label_location_final_path->setText( path
                                                  + QDir::separator()
                                                  + ((ui->checkBox_name_make_it_fat32->isChecked())
                                                     ? this->ui->lineEdit_name_clean->text()
                                                     : minimal_clean_name )
                                                  + QDir::separator()
                                                  + tr("<gnurpgm files>") );
    this->ui->pushButton_next->setEnabled( true );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void new_project_Form::on_pushButton_name_more_options_clicked()
{
    this->ui->label_name_usb_shit_description->setVisible( !ui->label_name_usb_shit_description->isVisible() );
    this->ui->checkBox_name_make_it_fat32->setVisible( !ui->checkBox_name_make_it_fat32->isVisible() );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
QString new_project_Form::get_gis_from_summary()
{

    gis_stream_object gis;
    // the first step lol
    gis.mkdir( "" ); // creates a path equal to this::get_project_base_path()

    // logo
    gis.copy_file_from_extern( this->project_logo_absolute_path, "logo.png" );

    // project minimal file
    gis.touch_file( "project.gnurpgm" );
    gis.populate_file( "project.gnurpgm",
                       "{ \"projectName\": \"" + ui->lineEdit_name_dirty->text() + "\" }" );

    // create basic folder structure
    gis.mkdir( "maps" );
    gis.mkdir( "res" );
    gis.mkdir( "res/" );
    gis.mkdir( "res/animations" );
    gis.mkdir( "res/pictures" );
    gis.mkdir( "res/textures" );
    gis.mkdir( "res/tilesets" );
    gis.mkdir( "res/panoramas" );
    gis.mkdir( "res/music" );
    gis.mkdir( "res/soundeffects" );
    gis.mkdir( "res/videos" );
    gis.mkdir( "sys" );
    gis.mkdir( "sys/mods" );
    gis.mkdir( "sys/mods/img" );
    gis.mkdir( "sys/matrix" );
    gis.mkdir( "sys/matrix/stores" );
    gis.mkdir( "sys/matrix/templates" );
    gis.mkdir( "sys/templates" );
    gis.mkdir( "sys/xvars" );

    return gis.get_gis();
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
QString new_project_Form::get_project_base_path()
{
    return ui->lineEdit_location_folder->text()
            + QDir::separator()
            + ui->lineEdit_name_dirty->text()
            + QDir::separator() ;
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void new_project_Form::on_pushButton_visit_online_tuts_clicked()
{
    QDesktopServices::openUrl( QUrl( "http://gnurpgm.sf.net/getstarted/tutorials.php" ) );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void new_project_Form::setup_toolbarBtns()
{
    QList<QPair <bool,QAction*> > toolbarBtns;

    toolbarBtns.append( QPair<bool,QAction*>(true,0));

    toolbarBtns.append( QPair<bool,QAction*>(false, new QAction( QIcon(":/icons/package-remove.png"),
                                                                 tr("Back to welcome screen"),
                                                                 gnurpgm._mainwindow->toolbarActionGroup)) );

    toolbarBtns.append( QPair<bool,QAction*>(true,0));

    connect( toolbarBtns[1].second, SIGNAL(triggered()),
             this, SLOT(goBack_to_welcomeScreen()) );

    gnurpgm.setMainWindowToolbar( toolbarBtns );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void new_project_Form::goBack_to_welcomeScreen()
{
    gnurpgm._mainwindow->gotoForm( MainWindow::WELCOME_PAGE );
}
//-----------------------------------------------------------------------------
