#ifndef NEW_PROJECT_FORM_H
#define NEW_PROJECT_FORM_H

#include <QFrame>
#include <QFont>
#include <QFileDialog>

#include "include/gis_stream_object.h"

//-----------------------------------------------------------------------------
namespace Ui {
class new_project_Form;
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
class new_project_Form : public QFrame
{
    Q_OBJECT

public:
    explicit new_project_Form(QWidget *parent = 0);
    ~new_project_Form();

    void restart();

    QStringList get_new_project_basic_fileslist();

    QStringList execution_todos;

    QString get_new_project_absolute_path();

    QString project_logo_absolute_path;

    void setup_toolbarBtns();

private:

    QString get_gis_from_summary();

    QString get_project_base_path();

public slots:
    void goBack_to_welcomeScreen();

private slots:
    void on_pushButton_next_clicked();

    void on_pushButton_previous_clicked();

    void on_pushButton_abort_clicked();

    void make_current_step_bold(int step);

    void apply_logic_for_step(int step);

    void execution_start();

    void execution_migration_start();

    void execution_downloads_start();

    void on_lineEdit_name_dirty_textChanged(const QString &arg1);

    void on_checkBox_name_make_it_fat32_toggled(bool checked);

    void on_pushButton_pick_logo_clicked();

    void on_pushButton_pick_location_folder_clicked();

    void on_pushButton_name_more_options_clicked();

    void on_pushButton_visit_online_tuts_clicked();

signals:
    void quit_me();

    void new_project_created( QString path_to_new_project_dot_gnurpgm );

private:
    Ui::new_project_Form *ui;
};
//-----------------------------------------------------------------------------

#endif // NEW_PROJECT_FORM_H
