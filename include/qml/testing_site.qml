import QtQuick 2.1
import QtQuick.Controls 1.1
import "qrc:content"

Rectangle {
    id: absoluteRootElement
    visible: true

    gradient: Gradient {
             GradientStop { position: 0.0; color: "#1e5799" }
             GradientStop { position: 0.1; color: "#207cca" }
             GradientStop { position: 0.2; color: "#2989d8" }
             GradientStop { position: 1.0; color: "#7db9e8" }
         }


    // Implements back key navigation
    Keys.onReleased: {
        if (event.key === Qt.Key_Back) {
            /*if (stackView.depth > 1) {
                stackView.pop();
                event.accepted = true;
            } else {*/ Qt.quit(); /*}*/
        }
    }


//    Best {
//        id: mainMenuSidebarz
//    }

    Item {
        id: centralWidget
        anchors.centerIn: parent.Center
        width: 400
        height: 400
        Row {
            Item {
                id: item1

                Image {
                    id: item1icon
                    source: "qrc:///img/project_new.png"
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    width: 40
                    height: 40

                }

                Text {
                    id: item1text
                    text: "wow much text"
                    width: 0
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: item1icon.right
                }

                MouseArea {
                    anchors.fill: parent
                    onEntered: item1text.width = parent.width
                    onExited: item1text.width = 0
                }
            }
        }
    }




    Item {
        id: projectActivitiesSidebar
        width: 300
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom


        BorderImage {
            anchors.fill: parent
            source: "qrc:///img/mm_bg.png"
            z: 1
        }

        Item {
            id: activitiestitlebar
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            height: 60
            z: 5

            BorderImage {
                source: "qrc:///img/wood_label.png"
                anchors.fill: parent
            }

            Text {
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                text: "Activities"
                color: "white"
                font.bold: true
                font.pixelSize: 16
                style: Text.Outline;
                styleColor: "black"
            }
        }


        ListModel {
                id: activitiesModel
                ListElement {
                    name: "Dude joined ur project"
                }
        }

        ListView {
            id: listview_activities
            model: activitiesModel
            anchors.top: activitiestitlebar.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            z: 2

            delegate: Item {
                id: item_activity
                width: parent.width
                height: 60

                signal clicked

                BorderImage {
                    anchors.fill: parent
                    source: "qrc:///img/wood_label.png"
                }
                Text {
                    color: "white"
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: name
                    style: Text.Outline;
                    styleColor: "black"
                    font.bold: true
                }
                Image {
                    source: "qrc:///img/navigation_next_item.png"
                    anchors.right: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                }
                MouseArea {
                    id: mouse_activity
                    anchors.fill: parent
                    onClicked: item_activity.clicked()
                }

            }

        }
    }
}

