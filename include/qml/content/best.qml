import QtQuick 2.0

Item {
    id: mainMenuSidebar
    anchors.top: parent.top
    anchors.left: parent.left
    //anchors.right: parent.right
    width: 300
    anchors.bottom: parent.bottom

    BorderImage {
        anchors.fill: parent
        source: "qrc:///img/mm_bg.png"
        z: 1
    }

    Item {
        id: titlebarthing
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        height: 60
        z: 5

        BorderImage {
            source: "qrc:///img/wood_label.png"
            anchors.fill: parent
        }

        Text {
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            text: "I'm the project title"
            color: "white"
            font.bold: true
            font.pixelSize: 16
            style: Text.Outline;
            styleColor: "black"
        }
    }

    ListModel {
            id: pageModel
            ListElement {
                name: "Maps"
                icon: "qrc:///img/menu_maps.png"
            }
            ListElement {
                name: "Matrix"
                icon: "qrc:///img/menu_matrix.png"
            }
            ListElement {
                name: "Resources"
                icon: "qrc:///img/menu_resources.png"
            }
            ListElement {
                name: "Tools"
                icon: "qrc:///img/menu_tools.png"
            }
            ListElement {
                name: "Details"
                icon: "qrc:///img/menu_projectDetails.png"
            }
            ListElement {
                name: "Play"
                icon: "qrc:///img/menu_projectDetails.png"
            }
        }

    ListView {
        id: listview
        model: pageModel
        anchors.top: titlebarthing.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        z: 2

        delegate: Item {
            id: item
            width: parent.width
            height: 60

            signal clicked

            BorderImage {
                anchors.fill: parent
                source: "qrc:///img/wood_label.png"
            }

            Image {
                source: icon
                fillMode: Image.PreserveAspectFit
                height: 50
                width: 100
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
            }
            Text {
                color: "white"
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                text: name
                style: Text.Outline;
                styleColor: "black"
                font.bold: true
            }
            Image {
                source: "qrc:///img/navigation_next_item.png"
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
            }
            MouseArea {
                id: mouse
                anchors.fill: parent
                onClicked: item.clicked()

            }

        }

    }
}
