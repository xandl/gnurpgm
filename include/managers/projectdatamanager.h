#ifndef PROJECTDATAMANAGER_H
#define PROJECTDATAMANAGER_H

#include <QObject>
#include <QVariant>

#include "include/managers/projectmatrixmanager.h"
#include "include/managers/projectactivitiesmanager.h"
#include "include/managers/projectmodmanager.h"


class projectDataManager : public QObject
{
    Q_OBJECT
public:
    explicit projectDataManager(QObject *parent = 0);

    QString project_name;

    QString project_base_dir;

/*!
 * Activiites handl events like "Your team mate updated
 * map xyz"
*/
    projectActivitiesManager activities;

    projectMatrixManager matrix;

    projectModManager mods;


signals:
    // much Versioncontrol integration?
    // void someOriginalFileChanged?
};

#endif // PROJECTDATAMANAGER_H
