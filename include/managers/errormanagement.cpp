#include "errormanagement.h"

#include <QMessageBox>
#include "include/greencastlesingleton.h"

errorManagement::errorManagement(QObject *parent) :
    QObject(parent)
{
}

void errorManagement::push(QString text, msgInfo types)
{
    _journal.append( QPair<msgInfo,QString>(types,text) );
    if (types.contains( errorManagement::IMPORTANT ))
    {
        QMessageBox* msgBox = new QMessageBox( gnurpgm._mainwindow );
        msgBox->setText( text );
        msgBox->setModal( true );

        if ( types.contains( errorManagement::CRITICAL ))
        {
            msgBox->setIcon( QMessageBox::Critical );
        }
        else if ( types.contains( errorManagement::WARNING ))
        {
            msgBox->setIcon( QMessageBox::Warning );
        }
        else
        {
            msgBox->setIcon( QMessageBox::Information );
        }

        msgBox->exec();
        delete msgBox;
    }
}


QStringList errorManagement::getJournalMessages(qint64 from, qint64 to)
{
    if ( from > _journal.count())
        return QStringList();

    QStringList msgs;
    qint64 max = ((to != -1) ? to : _journal.size() );
    for ( int i = 0; i < max; i++)
    {
        msgs.append(_journal[i].second);
    }

    return msgs;
}
