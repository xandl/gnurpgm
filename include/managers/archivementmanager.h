#ifndef ARCHIVEMENTMANAGER_H
#define ARCHIVEMENTMANAGER_H

#include <QObject>
#include <QVariantMap>
#include <QPair>

class archivementManager : public QObject
{
    Q_OBJECT

    struct sArchivement
    {
        QString uid;
        QString name;

    };

/*!
 *
 * ["gnurpgmOpened"] = int; each time gnurpgm has been opened, after 100: Archivement: Apprentice 1000: Archivement: Veteran
 * ["workspaceActionsTaken"] = int; each time some file changes this gets +1, after 1000: Archivement
 * ["workspaceClassicTimesOpened"] = int; how often the classic map editor has been opened
 * ["workspacePointAndClickTimesOpened"] = int; how often the point and click map editor has been opened
 * ["workspace3DTimesOpened"] = int; how often the 3d map editor has been opened
*/
    QVariantMap _archivedStatistics;

public:
    archivementManager(QObject *parent = 0);

    QPair<bool,QString> loadArchivements();

    QPair<bool,QString> saveArchivements();

signals:

public slots:

};

#endif // ARCHIVEMENTMANAGER_H
