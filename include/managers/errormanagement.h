#ifndef ERRORMANAGEMENT_H
#define ERRORMANAGEMENT_H

#include <QObject>
#include <QStringList>

#define msgInfo QList<errorManagement::MessageTypes>

class errorManagement : public QObject
{
    Q_OBJECT

public:
    explicit errorManagement(QObject *parent = 0);

    enum MessageTypes
    {
        NOT_AN_ERROR, // whatever happened
        WARNING,
        CRITICAL,
        IMPORTANT, // so important, we show a messagebox
        PROJECT_LOADING,
        RESSOURCE_AQUIRE_ERROR, // a file 404'd
        SCRIP_ERROR, // a script couldn't be parsed
        MAP_ERROR, // something went wrong in a map
        MATRIX_ERROR, // json database broken?
        MOD_ERROR // modification broken
    };

private:
    QList< QPair< msgInfo, QString> > _journal;

public slots:
    void push(QString warning = "", msgInfo types = msgInfo() << errorManagement::NOT_AN_ERROR);

    QStringList getJournalMessages(qint64 from = 0, qint64 to = -1);

};

#endif // ERRORMANAGEMENT_H
