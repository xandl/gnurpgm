#include "projectmatrixmanager.h"

#include <QDir>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QFile>
#include <QDebug>

#include "include/greencastlesingleton.h"

//---------------------------------------------------------------------------
projectMatrixManager::projectMatrixManager(QObject *parent) :
    QObject(parent)
{
    _changed = false;
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
bool projectMatrixManager::changed()
{
    return _changed;
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
QPair<bool, QString> projectMatrixManager::loadMatrixFromFolder(QString absoluteFolderPath)
{
    QDir matrixDir( absoluteFolderPath );
    if ( !matrixDir.exists() )
    {
        qDebug() << "projectMatrixManager::loadMatrixFromFolder(QString" <<  absoluteFolderPath << "): folder doesnt exist";
        return QPair<bool, QString>(false, tr("Folder doesn't exist: %1").arg(absoluteFolderPath) );
    }
    _absoluteMatrixFolderPath = absoluteFolderPath;

    // loading all found
    // *.topic.json
    // *.elements.json
    // files

    QStringList topic_files = matrixDir.entryList( QStringList() << "*.topic.json", QDir::Files | QDir::NoDotAndDotDot, QDir::Name );
    foreach (QString relativeFileName, topic_files)
    {
        QByteArray data;
        QPair<bool,QString> report = gnurpgm._readDataFromFile( absoluteFolderPath + QDir::separator() + relativeFileName,
                                                                &data );
//        if (report.first)sads
    }

    // dont forget to check, each time we associate element data with an topic
    // to check if that topic exists at all
    // if we associate it and it doesnt even exist - Q_ASSERT_Xs ... Q_ASSERT_Xs everywhere!
    QStringList element_files = matrixDir.entryList( QStringList() << "*.elements.json", QDir::Files | QDir::NoDotAndDotDot, QDir::Name );

    _changed = false;
    return QPair<bool, QString>(true,"");
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void projectMatrixManager::addNewTopic(QString newTopicName)
{
    Q_ASSERT_X( !_topics.contains(newTopicName),
                "projectMatrixManager::addNewTopic()",
                "Topic already exists.");

    _changed = true;
    _topics.insert(newTopicName, QList<sTopicEntry>() );
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void projectMatrixManager::addIdToTopic(QString topicName, QString idName, QString description, MatrixInputTypes inputType)
{
    Q_ASSERT_X( _topics.contains(topicName),
                "projectMatrixManager::addIdToTopic()",
                "Topic doesnt exist.");

    // Remove the old id (if there already existed one)
    removeIdFromTopic(topicName, idName);

    QMap<QString,QList< sTopicEntry> >::Iterator topIt;
    for ( topIt  = _topics.begin();
          topIt != _topics.end();
          ++topIt )
    {
        if (topIt.key() == topicName)
        {
            topIt.value().append( (sTopicEntry){idName, description, inputType} );
            break;
        }
    }

    // i think we can assume that this function changed ... something by now ;-)
    _changed = true;
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
QList<projectMatrixManager::sTopicEntry> projectMatrixManager::getIdsFromTopic(QString topicName)
{
    QList<projectMatrixManager::sTopicEntry> ids;

    QMap<QString,QList< sTopicEntry> >::Iterator topIt;
    for ( topIt  = _topics.begin();
          topIt != _topics.end();
          ++topIt )
    {
        if (topIt.key() == topicName)
        {
            for (int i = 0; i < topIt.value().count(); i++)
                ids.append(topIt.value().at(i));
        }
    }

    return ids;
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
QStringList projectMatrixManager::getAllTopicNames()
{
    QStringList topicNames;

    QMap<QString,QList< sTopicEntry> >::Iterator topIt;
    for ( topIt  = _topics.begin();
          topIt != _topics.end();
          ++topIt )
    {
        topicNames.append( topIt.key() );
    }

    return topicNames;
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void projectMatrixManager::removeIdFromTopic(QString topicName, QString idName)
{
    // we just remove it from the head.
    // even if it remains in the data, it wont be loaded anyways and
    // gets droped after one of the elements has been saved again

    // we have to iterate through the topics too, because
    // QMap's value(topicName) return some const result
    // from which we can't call removeAt(i)
    // fml but it works...

    QMap<QString,QList< sTopicEntry> >::Iterator topIt;
    for (topIt  = _topics.begin();
         topIt != _topics.end();
         ++topIt )
        if (topIt.key() == topicName)
        {
            for (int i = 0; i < topIt.value().count(); i++ )
            if (topIt.value().at(i).entryName == idName )
            {
                // die die die !....
                topIt.value().removeAt(i);
                i--;
            }
        }
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void projectMatrixManager::removeTopicAndItsElements(QString topicName)
{
    // 1) remove topic head
    _topics.remove(topicName);

    // 2) remove assiociated topic elements
    _elements.remove(topicName);
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void projectMatrixManager::addElementToTopic(QString topicName, QVariantMap ids_and_values_element)
{
    Q_ASSERT_X( ids_and_values_element.contains("name"),
                "projectMatrixManager::addElementToTopic()",
                "That element has no \"name\" attribute -> fahk dat" );

    QMap<QString, QMap< QString,QVariantMap > >::Iterator topIt;
    for ( topIt  = _elements.begin();
          topIt != _elements.end();
          ++topIt )
    {
        if (topIt.key() == topicName)
        {
            topIt.value().insert( ids_and_values_element.value("name").toString(),
                                  ids_and_values_element );
        }
    }
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void projectMatrixManager::setElementInTopic(QString topicName, QString elementName, QVariantMap values)
{
    QMap<QString, QMap< QString,QVariantMap > >::Iterator topIt;
    for ( topIt  = _elements.begin();
          topIt != _elements.end();
          ++topIt )
    {
        if (topIt.key() == topicName)
        {
            // ok, let's fetch dem elemetz
            QMap< QString,QVariantMap >::Iterator elementsIt;
            for ( elementsIt  = topIt.value().begin();
                  elementsIt != topIt.value().end();
                  ++elementsIt )
            {
                // element? is det you?
                if (elementsIt.key() == elementName)
                {
                    // found it, lelz
                    elementsIt.value() = values;
                }
            }
        }
    }
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
QVariantMap projectMatrixManager::getElementFromTopic(QString topicName, QString elementName)
{
    QMap<QString, QMap< QString,QVariantMap > >::Iterator topIt;
    for ( topIt  = _elements.begin();
          topIt != _elements.end();
          ++topIt )
    {
        if (topIt.key() == topicName)
        {
            // ok, let's fetch dem elemetz
            QMap< QString,QVariantMap >::Iterator elementsIt;
            for ( elementsIt  = topIt.value().begin();
                  elementsIt != topIt.value().end();
                  ++elementsIt )
            {
                // element? is det you?
                if (elementsIt.key() == elementName)
                {
                    // found it, lelz
                    return elementsIt.value();
                }
            }
        }
    }

    return QVariantMap();
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void projectMatrixManager::removeElementFromTopic(QString topicName, QString elementName)
{
    QMap<QString, QMap< QString,QVariantMap > >::Iterator topIt;
    for ( topIt  = _elements.begin();
          topIt != _elements.end();
          ++topIt )
    {
        if (topIt.key() == topicName)
        {
            // ok, let's fetch dem elemetz
            QMap< QString,QVariantMap >::Iterator elementsIt;
            for ( elementsIt  = topIt.value().begin();
                  elementsIt != topIt.value().end();
                  ++elementsIt )
            {
                if (elementsIt.key() == elementName)
                {
                    elementsIt = topIt.value().erase(elementsIt);
                    break;
                }

            }
        }
    }
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
QString projectMatrixManager::topicsToJSON()
{
    QJsonArray allTopicsArray;

    QMap<QString,QList< sTopicEntry> >::Iterator topIt;
    for ( topIt  = _topics.begin();
          topIt != _topics.end();
          ++topIt )
    {
        QJsonValue topicName = topIt.key();
        QJsonArray topicIds;

        for( int i = 0; i < topIt.value().count(); i++)
        {
            QJsonObject obj;
            obj["id"] = topIt.value().at(i).entryName;
            obj["description"] = topIt.value().at(i).userDescription;
            obj["type"] = topIt.value().at(i).inputType;
            topicIds.append( obj );
        }

        QJsonObject topic;
        topic["name"] = topicName;
        topic["ids"] = topicIds;

        allTopicsArray.append(topic);
    }

    QJsonDocument jDoc( allTopicsArray );
    return jDoc.toJson(QJsonDocument::Compact);
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
QString projectMatrixManager::topicToJSON(QString topicName)
{
    return topicToJSON(topicName, QJsonDocument::Compact);
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
QString projectMatrixManager::topicToJSON(QString topicName, QJsonDocument::JsonFormat jsonFormat)
{
    QJsonObject topic;

    QMap<QString,QList< sTopicEntry> >::Iterator topIt;
    for ( topIt  = _topics.begin();
          topIt != _topics.end();
          ++topIt )
    {
        if ( topIt.key() == topicName)
        {
            QJsonValue topicName = topIt.key();
            QJsonArray topicIds;

            for( int i = 0; i < topIt.value().count(); i++)
            {
                QJsonObject obj;
                obj["id"] = topIt.value().at(i).entryName;
                obj["description"] = topIt.value().at(i).userDescription;
                obj["type"] = topIt.value().at(i).inputType;
                topicIds.append( obj );
            }

            topic["name"] = topicName;
            topic["ids"] = topicIds;
            break;
        }
    }

    QJsonDocument jDoc( topic );
    return jDoc.toJson( jsonFormat );
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
QString projectMatrixManager::elementsToJSON(QString topicName)
{
    return elementsToJSON(topicName, QJsonDocument::Compact);
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
QString projectMatrixManager::elementsToJSON(QString topicName, QJsonDocument::JsonFormat format)
{
    Q_ASSERT_X( _topics.contains(topicName),
                "projectMatrixManager::elementsToJSON()",
                "Topic doesnt exist.");

    QJsonArray allElementsArray;

    QMap<QString, QMap< QString,QVariantMap > >::Iterator overtIt;
    for ( overtIt  = _elements.begin();
          overtIt != _elements.end();
          ++overtIt )
    {
        if (overtIt.key() == topicName)
        {
            // ok, let's fetch dem elemetz
            QMap< QString,QVariantMap >::Iterator elementsIt;
            for ( elementsIt  = overtIt.value().begin();
                  elementsIt != overtIt.value().end();
                  ++elementsIt )
            {
                // ok, let's fetch some element

                QJsonValue elementName = elementsIt.key();
                QJsonArray elementValues;

                QVariantMap::Iterator elemIt;
                for ( elemIt  = elementsIt.value().begin();
                      elemIt != elementsIt.value().end();
                      ++elemIt )
                {
                    QJsonObject obj;
                    obj["key"] = elemIt.key();
                    obj["value"] = elemIt.value().toJsonValue();
                    elementValues.append( obj );
                }

                QJsonObject element;
                element["name"] = elementName;
                element["values"] = elementValues;

                allElementsArray.append(element);
            }
        }
    }

    QJsonDocument jDoc( allElementsArray );
    return jDoc.toJson(format);
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
QString projectMatrixManager::absoluteTopicsFilename(QString topicName)
{
    return _absoluteMatrixFolderPath + QDir::separator() + topicName.toLower() + ".topic.json";
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
QString projectMatrixManager::absoluteElementsFilename(QString topicName)
{
    return _absoluteMatrixFolderPath + QDir::separator() + topicName.toLower() + ".elements.json";
}
//---------------------------------------------------------------------------

