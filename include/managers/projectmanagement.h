#ifndef PROJECTMANAGEMENT_H
#define PROJECTMANAGEMENT_H

#include <QObject>
#include "include/managers/projectdatamanager.h"

class projectManagement : public QObject
{
    Q_OBJECT

    projectDataManager* _currentProject;

public:
    explicit projectManagement(QObject *parent = 0);

    projectDataManager* currentProject();

    QMap<QString,QString> recentProjects;

    void loadProject(QString absoluteFilePath);

    QPair<bool,QString> saveCurrentProject();

    QPair<bool,QString> closeCurrentProject(bool saveIt);

signals:

public slots:
    void loadProject_maps();

    void loadProject_mods();

    void loadProject_matrix();

    void loadProject_xvars();

    void loadProject_news();

    void loadProject_done();

};

#endif // PROJECTMANAGEMENT_H
