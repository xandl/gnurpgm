#include "projectmanagement.h"

#include <QFile>
#include <QDebug>
#include <QMessageBox>
#include <QJsonParseError>
#include <QJsonObject>
#include <QJsonValue>
#include <QFileInfo>
#include <QDir>
#include <QTimer>

#include "include/greencastlesingleton.h"

projectManagement::projectManagement(QObject *parent) :
    QObject(parent)
{
    _currentProject = 0;
}

projectDataManager* projectManagement::currentProject()
{
//    Q_ASSERT_X( _currentProject != 0,
//                "projectManagement::currentProject()",
//                "_currentProject is NULL" );
    return _currentProject;
}

void projectManagement::loadProject(QString absoluteFilePath)
{
    gnurpgm._mainwindow->gotoForm( MainWindow::LOADING_SCREEN );

    QByteArray data;
    QPair<bool,QString> result = gnurpgm._readDataFromFile( absoluteFilePath, &data );

    errorsManager->push( result.second,
                         msgInfo() << errorManagement::PROJECT_LOADING
                         << errorManagement::CRITICAL
                         << errorManagement::IMPORTANT );

    QJsonParseError jParseError;
    QJsonDocument jDoc = QJsonDocument::fromJson( data, &jParseError );

    if (jParseError.error != QJsonParseError::NoError)
    {
        QMessageBox::critical( 0,
                               tr("Error when parsing project file"),
                               tr("An Error appeared when parsing the project data:\n%1")
                               .arg(jParseError.errorString()));

        gnurpgm._mainwindow->gotoForm( MainWindow::WELCOME_PAGE );

//        return QPair<bool,QString>(false,jParseError.errorString());
    }

    // start parsing pardeeeyyy
    //todo
    projectDataManager* project = new projectDataManager();

    project->project_name = jDoc.object().value("projectName").toString();

    QFileInfo finfo(absoluteFilePath);
    project->project_base_dir = finfo.absoluteDir().absolutePath();

    _currentProject = project;

    QTimer::singleShot( 300, this, SLOT(loadProject_maps()) );

//    return QPair<bool,QString>(true,"");
}

QPair<bool,QString> projectManagement::saveCurrentProject()
{
    Q_ASSERT_X( _currentProject != NULL,
                "projectManagement::saveCurrentProject()",
                "_currentProject is NULL" );

    // what to save:
    // matrix
    // project data

    // Has something in the matrix been changed?
    if ( currentProject()->matrix.changed() )
    {
        // ok let's save those changes
        // (and i mean save all of it - or else
        // i maybe should have used SQLite for this
        // in the first place...)

        foreach ( QString topicName, _currentProject->matrix.getAllTopicNames() )
        {
            QPair<bool,QString> topicsSave
                    = gnurpgm._writeDataToFile( _currentProject->matrix.absoluteTopicsFilename(topicName),
                                                _currentProject->matrix.topicsToJSON().toUtf8());

            // all went well?
            if (topicsSave.first)
            {
                // ok let's save the elements too!
                QPair<bool,QString> elementsSave
                        = gnurpgm._writeDataToFile( _currentProject->matrix.absoluteTopicsFilename(topicName),
                                                    _currentProject->matrix.topicsToJSON().toUtf8());

                // did something went wrong?
                if (!elementsSave.first)
                {
                    qDebug() << "projectManagement::saveCurrentProject(): " << elementsSave.second;
                    QMessageBox::critical(0, tr("Error saving project data"), elementsSave.second);
                    return elementsSave;
                }
            }
            else
            {
                // ok, lets display the error message and also write it to the console
                qDebug() << "projectManagement::saveCurrentProject(): " << topicsSave.second;
                QMessageBox::critical(0, tr("Error saving project data"), topicsSave.second);
                return topicsSave;
            }
        }


    }
    return QPair<bool,QString>(true,"");
}

QPair<bool,QString> projectManagement::closeCurrentProject(bool saveIt)
{
    if( saveIt )
    {
        // let's try to save the project
        QPair<bool,QString> result = saveCurrentProject();

        // went something wrong?
        if (!result.first)

            // let's return what went wrong
            return result;
    }

    // actual action
    delete _currentProject;
    _currentProject = NULL;

    // all went good, no need to return a message
    gnurpgm._mainwindow->gotoForm( MainWindow::WORKSPACE );
    return QPair<bool,QString>(true,"");
}

void projectManagement::loadProject_maps()
{
    QTimer::singleShot( 300, this, SLOT(loadProject_matrix()) );
}

void projectManagement::loadProject_matrix()
{
    QTimer::singleShot( 300, this, SLOT(loadProject_mods()) );
}

void projectManagement::loadProject_mods()
{
    QTimer::singleShot( 300, this, SLOT(loadProject_xvars()) );
}

void projectManagement::loadProject_xvars()
{
//    gnurpgm._mainwindow->welcomeForm
    QTimer::singleShot( 300, this, SLOT(loadProject_news()) );
}

void projectManagement::loadProject_news()
{
//    gnurpgm._mainwindow->welcomeForm
    QTimer::singleShot( 300, this, SLOT(loadProject_done()) );
}

void projectManagement::loadProject_done()
{
    gnurpgm._mainwindow->gotoForm( MainWindow::WORKSPACE );
}
