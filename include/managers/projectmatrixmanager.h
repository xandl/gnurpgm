#ifndef PROJECTMATRIXMANAGER_H
#define PROJECTMATRIXMANAGER_H

#include <QObject>
#include <QMap>
#include <QVariant>
#include <QJsonDocument>

class projectMatrixManager : public QObject
{
    Q_OBJECT

    bool _changed;

    QString _absoluteMatrixFolderPath;

    enum MatrixInputTypes
    {
        M_UNDEFINED,
        M_BOOLEAN_INPUT,
        M_INTEGER_INPUT,
        M_TEXT_INPUT,
        M_CV_SWITCH_SELECTOR,
        M_CV_INTEGER_SELECTOR,
        M_CV_TEXT_SELECTOR,
        M_IMAGE_SELECTOR,
        M_FILE_SELECTOR
    };

    struct sTopicEntry
    {
        QString entryName;
        QString userDescription;
        MatrixInputTypes inputType;
    };

/*!
 * QMap<QString,QList< sTopicEntry> > describes as follows:
 * QMap<TopicName,QList< sTopicEntry=allTheShitInATopic> >
 *
 * Think of sTopicEntry as a Table-Description as it is in SQL-Tabels
 */

    QMap<QString,QList< sTopicEntry> > _topics;

/*!
 * QMap<QString,QMap< QString, QVariant> > describes as follows:
 * QMap<TopicName, QList< QMap< id,value > > >
 *
 * For example:
 * "Heroes"
 *  [0]
 *   |---> "name", "Dude"
 *   |---> "baseHealth", "12"
 *   \---> "baseMagic", "7"
 * "Heroes"
 *  [0]
 *   |---> "name", "Sir Graham of Daventry"
 *   |---> "baseHealth", "1" // srsly he always dies somehow
 *   \---> "baseMagic", "9000"
 *
*/
    QMap<QString, QMap< QString,QVariantMap > > _elements;

public:
    explicit projectMatrixManager(QObject *parent = 0);

    QPair<bool,QString> loadMatrixFromFolder(QString absoluteFolderPath);

    bool changed();

    // --- TOPICs helpers ------------------------

    void addNewTopic(QString newTopicName);

    void addIdToTopic(QString topicName, QString idName, QString description, MatrixInputTypes inputType);

    QList<sTopicEntry> getIdsFromTopic(QString topicName);

    QStringList getAllTopicNames();

    void removeIdFromTopic(QString topicName, QString idName);

    void removeTopicAndItsElements(QString topicName);

    // --- ELEMENTs helpers ------------------------

    void addElementToTopic(QString topicName, QVariantMap ids_and_values_element);

    void setElementInTopic(QString topicName, QString elementName, QVariantMap values);

    QVariantMap getElementFromTopic(QString topicName, QString elementName);

    void removeElementFromTopic(QString topicName, QString elementName);


    // --- FILE/SAVE helpers ------------------------
/*!
 * For saving data.
*/
    QString topicsToJSON();

    QString topicToJSON(QString topicName);

    QString topicToJSON(QString topicName, QJsonDocument::JsonFormat format);

    bool jsonToTopic(QString json);


    QString elementsToJSON(QString topicName);

    QString elementsToJSON(QString topicName, QJsonDocument::JsonFormat format);


    QString absoluteTopicsFilename(QString topicName);

    QString absoluteElementsFilename(QString topicName);

};

#endif // PROJECTMATRIXMANAGER_H
