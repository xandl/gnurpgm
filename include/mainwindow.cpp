#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QToolBar>
#include <QLayout>
#include "include/greencastlesingleton.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->welcomeForm = new _testingWelcomeForm();
    this->newProjectAssistant = new new_project_Form();
    this->workspaceForm = new _testingWotkspaceForm();
    this->preferencesForm = new _testingPreferencesDialog();
    this->toolbarActionGroup = new QActionGroup(this);

    this->setContextMenuPolicy(Qt::NoContextMenu);

    ui->stackedWidget->addWidget(welcomeForm);
    ui->stackedWidget->addWidget(newProjectAssistant);
    ui->stackedWidget->addWidget(workspaceForm);
    ui->stackedWidget->addWidget(preferencesForm);

    connect( this->newProjectAssistant, SIGNAL(new_project_created(QString)),
             this, SLOT(newProAssistant_createdNewProject_soWeOpenItNow(QString)));

#ifdef Q_OS_ANDROID
    ui->toolBar->setIconSize(QSize(42,42));
    ui->menuBar->setNativeMenuBar(false);
#endif


#ifdef Q_OS_MAC
    this->setWindowTitle("");
#endif
}

MainWindow::~MainWindow()
{
    delete ui;
}

QToolBar* MainWindow::toolbar()
{
    return ui->toolBar;
}

void MainWindow::gotoForm(mainForms targetForm)
{
    switch(targetForm)
    {
    case WELCOME_PAGE:
        ui->stackedWidget->slideInIdx( ui->stackedWidget->indexOf( welcomeForm ) );
        welcomeForm->setup_toolbarBtns();
    break;
    case NEW_PROJECT_ASSISTANT:
        ui->stackedWidget->slideInIdx( ui->stackedWidget->indexOf( newProjectAssistant ) );
        newProjectAssistant->setup_toolbarBtns();
    break;
    case LOADING_SCREEN:
    break;
    case WORKSPACE:
        ui->stackedWidget->slideInIdx( ui->stackedWidget->indexOf( workspaceForm ) );
        workspaceForm->setup_toolbarBtns();
    break;
    case PREFERENCES:
        ui->stackedWidget->slideInIdx( ui->stackedWidget->indexOf( preferencesForm ) );
        preferencesForm->setup_toolbarBtns();
    break;
    }
}

void MainWindow::newProAssistant_createdNewProject_soWeOpenItNow(QString absolutPathToProjectFile)
{
    // we ignore the result because we only use it when
    // on project gets opened directly (per application startup-argument)
    gnurpgm.project_management->loadProject(absolutPathToProjectFile);
}

