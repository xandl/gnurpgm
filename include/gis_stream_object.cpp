#include "gis_stream_object.h"

//-----------------------------------------------------------------------------
gis_stream_object::gis_stream_object()
{
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void gis_stream_object::touch_file(QString relative_path)
{
    gis_stream.append( "touch( \"" + relative_path.remove("..") + "\" );" );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void gis_stream_object::mkdir(QString relative_folder_path)
{
    gis_stream.append( "mkdir( \"" + relative_folder_path.remove("..") + "\" );" );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void gis_stream_object::download_file(QString url, QString target_relative_file_location)
{
    gis_stream.append( "download( \"" + url + "\", \"" + target_relative_file_location + "\" );\n" );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void gis_stream_object::copy_file(QString src_relative_filename, QString out_relative_filename)
{
    gis_stream.append( "copy( \"" + src_relative_filename + "\", \"" + out_relative_filename + "\" );\n" );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void gis_stream_object::copy_file_from_extern(QString absolute_src_filepath, QString out_relative_filename)
{
    gis_stream.append( "copy_from_ex( \"" + absolute_src_filepath + "\", \"" + out_relative_filename + "\" );\n" );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void gis_stream_object::move_file(QString src_relative_filename, QString out_relative_filename)
{
    gis_stream.append( "move( \"" + src_relative_filename + "\", \"" + out_relative_filename + "\" );\n" );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void gis_stream_object::delete_file(QString relative_path)
{
    gis_stream.append( "delete( \"" +  relative_path+ "\" );\n" );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void gis_stream_object::populate_file(QString relative_target_filename, QString data)
{
    gis_stream.append( "populate( \"" + relative_target_filename + "\", \"" + data + "\" );\n" );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void gis_stream_object::populate_file_with_b64(QString relative_target_filename, QString data)
{
    gis_stream.append( "populate( \"" + relative_target_filename + "\", \"" + data + "\" );\n" );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void gis_stream_object::unzip_file(QString sourceZipFile_relative_filename, QString extraction_relativeLocationPrefix)
{
    gis_stream.append( "unzip( \"" + sourceZipFile_relative_filename + "\", \"" + extraction_relativeLocationPrefix + "\" );\n" );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
QString gis_stream_object::get_gis()
{
    return gis_stream;
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void gis_stream_object::reset_gis_stream()
{
    this->gis_stream.clear();
}
//-----------------------------------------------------------------------------
