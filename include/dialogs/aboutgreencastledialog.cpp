#include "aboutgreencastledialog.h"
#include "ui_aboutgreencastledialog.h"

aboutGreenCastleDialog::aboutGreenCastleDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::aboutGreenCastleDialog)
{
    ui->setupUi(this);
    this->setWindowFlags( Qt::Sheet );
}

aboutGreenCastleDialog::~aboutGreenCastleDialog()
{
    delete ui;
}

void aboutGreenCastleDialog::on_pushButton_clicked()
{
    this->accept();
}
