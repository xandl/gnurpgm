#ifndef ABOUTGREENCASTLEDIALOG_H
#define ABOUTGREENCASTLEDIALOG_H

#include <QDialog>

namespace Ui {
class aboutGreenCastleDialog;
}

class aboutGreenCastleDialog : public QDialog
{
    Q_OBJECT

public:
    explicit aboutGreenCastleDialog(QWidget *parent = 0);
    ~aboutGreenCastleDialog();

private slots:
    void on_pushButton_clicked();

private:
    Ui::aboutGreenCastleDialog *ui;
};

#endif // ABOUTGREENCASTLEDIALOG_H
