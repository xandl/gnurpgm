#ifndef GNURIS_INTERPRETER_FORM_H
#define GNURIS_INTERPRETER_FORM_H

#include <QWidget>
#include <QString>
#include <QFileDialog>

namespace Ui {
class gnuris_interpreter_Form;
}

class gnuris_interpreter_Form : public QWidget
{
    Q_OBJECT
    
public:
    explicit gnuris_interpreter_Form(QWidget *parent = 0);
    ~gnuris_interpreter_Form();
    
private slots:
    void on_toolButton_1_custom_path_clicked();

    void on_toolButton_2_gis_clicked();

    void on_pushButton_load_gis_clicked();

private:
    Ui::gnuris_interpreter_Form *ui;
};

#endif // GNURIS_INTERPRETER_FORM_H
