#include "gnurisstandalone_mainwindow.h"
#include "ui_gnurisstandalone_mainwindow.h"

gnurisStandAlone_MainWindow::gnurisStandAlone_MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::gnurisStandAlone_MainWindow)
{
    ui->setupUi(this);
}

gnurisStandAlone_MainWindow::~gnurisStandAlone_MainWindow()
{
    delete ui;
}
