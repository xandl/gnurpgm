#include <QApplication>
#include "include/greencastlesingleton.h"
#include "include/mainwindow.h"

#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    a.setApplicationName( "GNURPGM" );
    a.setApplicationDisplayName( "GNURPGM" );

    MainWindow w;
    w.show();

    gnurpgm.setMainWindowPointer(&w);

    gnurpgm.initialize();

//#ifdef Q_OS_ANDROID
//    w.hide();
//    _testingAndroidMainWindow mw;
//    mw.show();
//#endif



//    QByteArray data;
//    gnurpgm._readDataFromFile(":/qml/content/mainMenuSidebar.qml",
//                                &data);
//    qDebug() << data;

    return a.exec();
}
